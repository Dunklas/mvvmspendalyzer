package com.dunk.mvvmspendalyzer.bindings;

import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.databinding.InverseBindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.widget.RadioGroup;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.domain.CategoryType;
import com.dunk.mvvmspendalyzer.ui.common.category.CategoryAdapter;

import java.util.ArrayList;
import java.util.List;

/*
 * This class contain binding adapters and such related to Category objects.
 */
public class CategoryBindings {

    private CategoryBindings() {}

    /**
     * XML property app:categories.
     * Shows a list of Category objects in a RecyclerView.
     * Adapter must be of type CategoryAdapter.
     */
    @BindingAdapter("categories")
    public static void setCategories(RecyclerView recyclerView, List<Category> categories) {
        CategoryAdapter adapter = (CategoryAdapter) recyclerView.getAdapter();
        if (adapter != null && categories != null) {
            adapter.update(new ArrayList<>(categories)); // New list, to make sure the list is changed while updating
        }
    }

    /**
     * XML property android:checkedButton.
     * Used for two way binding between a RadioGroup and
     * a CategoryType field. This method converts the selected
     * button to a CategoryType object.
     */
    @InverseBindingAdapter(attribute = "android:checkedButton")
    public static CategoryType getType(RadioGroup radioGroup) {
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.category_addedit_expense_btn:
                return CategoryType.EXPENSE;
            case R.id.category_addedit_income_btn:
                return CategoryType.INCOME;
            default:
                return null;
        }
    }

    /**
     * XML property android:checkedButton.
     * Used for two way binding between a RadioGroup and
     * a CategoryType field. This method converts a
     * CategoryType object to a button id.
     */
    @BindingConversion
    public static int toLayout(CategoryType type) {
        if (type == null) {
            return -1;
        }
        switch (type) {
            case EXPENSE:
                return R.id.category_addedit_expense_btn;
            case INCOME:
                return R.id.category_addedit_income_btn;
            default:
                return -1;
        }
    }

}
