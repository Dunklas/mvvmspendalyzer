package com.dunk.mvvmspendalyzer.bindings;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.resources.AssetsProvider;
import com.dunk.mvvmspendalyzer.resources.AssetsProviderImpl;
import com.dunk.mvvmspendalyzer.ui.common.icon.IconAdapter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class IconBindings {

    private IconBindings() {}

    /**
     * XML property app:icons.
     * Shows a list of icons/drawables in a RecyclerView.
     * Adapter must be of type IconAdapter.
     */
    @BindingAdapter("icons")
    public static void setIcons(RecyclerView recyclerView, List<String> icons) {
        IconAdapter adapter = (IconAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.update(icons);
        }
    }

    /**
     * XML property app:icon.
     * Loads a drawable from iconName and
     * set it to imageView. If a drawable for iconName cannot
     * be found, a default image will be loaded.
     */
    @BindingAdapter("icon")
    public static void setIcon(ImageView imageView, String iconName) {
        AssetsProvider assetsProvider = AssetsProviderImpl.getInstance(imageView.getContext());
        assetsProvider.icon(iconName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(drawable -> imageView.setImageDrawable(drawable));
    }
}
