package com.dunk.mvvmspendalyzer.bindings;

import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.databinding.InverseBindingListener;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;

import com.dunk.mvvmspendalyzer.domain.Transaction;
import com.dunk.mvvmspendalyzer.logging.Tags;
import com.dunk.mvvmspendalyzer.ui.common.transaction.TransactionAdapter;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TransactionBindings {

    private TransactionBindings() {}

    /**
     * XML property app:transactions.
     * Shows a list of Transaction objects in a RecyclerView.
     * Adapter must be of type TransactionAdapter.
     */
    @BindingAdapter({"transactions"})
    public static void setTransactions(RecyclerView recyclerView, List<Transaction> transactions) {
        TransactionAdapter adapter = (TransactionAdapter) recyclerView.getAdapter();
        if (adapter != null && transactions != null) {
            adapter.update(new ArrayList<>(transactions));
        }
    }

    /**
     * XML property app:amountLabel.
     * Displays a string representation (including currency)
     * of amount in textView. Not suitable for
     * two way data binding (due to currency character).
     */
    @BindingAdapter("amountLabel")
    public static void setAmountLabel(TextView textView, BigDecimal amount) {
        if (amount != null) {
            String str = NumberFormat.getCurrencyInstance().format(amount);
            textView.setText(str);
        }
    }

    /**
     * XML property app:date.
     * Displays a localized string representation
     * of date in textView. Not suitable for two
     * way data binding (due to localization).
     */
    @BindingAdapter("date")
    public static void setDate(TextView textView, Calendar date) {
        if (date != null) {
            java.text.DateFormat df = DateFormat.getDateFormat(textView.getContext());
            textView.setText(df.format(date.getTime()));
        }
    }

    /**
     * XML property app:amount.
     * Sets a string representation of value to textView.
     *
     * If textView contains text, that text is parsed and compared to value.
     * If not equal, text of textView is updated.
     * Note that compareTo is used for comparison (strict equals not needed, since 0 == 0.00 in this case).
     */
    @BindingAdapter("amount")
    public static void setAmount(TextView textView, BigDecimal value) {
        if (value != null && textView != null) {
            boolean shouldSetNewValue = textView.getText().length() == 0;

            if (!shouldSetNewValue) {
                try {
                    BigDecimal bd = new BigDecimal(textView.getText().toString());
                    shouldSetNewValue = value.compareTo(bd) != 0;
                } catch (NumberFormatException nfe) {
                    Log.d(Tags.LOG_UI, "Could not parse " + textView.getText() + " to BigDecimal");
                }
            }

            if (shouldSetNewValue) {
                if (value != null) {
                    textView.setText(value.toString());
                }
            }
        }
    }

    /**
     * XML property app:amount.
     * Converts content of textView to a BigDecimal.
     * Note that NumberFormatException is only logged. UI is responsible
     * for constraining the textView, so that parsing is possible.
     */
    @InverseBindingAdapter(attribute = "amount")
    public static BigDecimal getAmount(TextView textView) {
        try {
            return new BigDecimal(textView.getText().toString());
        } catch (NumberFormatException nfe) {
            Log.d(Tags.LOG_UI, "Could not parse " + textView.getText() + " to BigDecimal");
        }
        return null;
    }

    /**
     * XML property app:amount.
     * This method is required to have the two above methods to fire.
     * See: https://developer.android.com/reference/android/databinding/InverseBindingAdapter.html
     */
    @BindingAdapter(value = "amountAttrChanged")
    public static void setAmountListener(TextView textView, final
                                         InverseBindingListener listener) {
        if (listener != null) {
            textView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

                @Override
                public void afterTextChanged(Editable editable) {
                    listener.onChange();
                }
            });
        }
    }
}
