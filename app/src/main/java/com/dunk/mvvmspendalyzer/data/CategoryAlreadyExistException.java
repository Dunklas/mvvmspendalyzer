package com.dunk.mvvmspendalyzer.data;

/**
 * Exception class used when an insert of a Category
 * object defies unique constraint.
 */
public class CategoryAlreadyExistException extends RuntimeException {

    public CategoryAlreadyExistException(String msg) {
        super(msg);
    }

    public CategoryAlreadyExistException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
