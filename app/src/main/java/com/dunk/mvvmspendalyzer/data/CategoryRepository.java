package com.dunk.mvvmspendalyzer.data;

import com.dunk.mvvmspendalyzer.domain.Category;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * A repository of Category objects.
 * Note! All of the methods in this interface may emit a CompositeException containing RepositoryException in onError().
 *
 * Any database or web query performed behind this interface will run on a background thread.
 * Consumer of this interface will need to run .observeOn(AndroidSchedulers.mainThread()) in
 * order to perform actions on mainThread.
 */
public interface CategoryRepository {

    /**
     * Returns a Flowable containing a List of all Category-objects in the repository.
     * onNext() will be called each time a change has occurred in the underlying category storage.
     * The list is sorted by type, name.
     */
    Flowable<List<Category>> categories();

    /**
     * Returns a Maybe which may contain a Category of id.
     * onSuccess() will be called if a Category of id exist, otherwise onComplete() will be called.
     *
     * @param   id  id of queried category
     */
    Maybe<Category> category(String id);

    /**
     * Returns a Completable, which on subscription will try to insert a new Category
     * to the repository. If successful, onComplete() will be called.
     * Note! onError() may emit a CompositeException containing CategoryAlreadyExistException if a category with the
     * same name already exist.
     *
     * @param   category    category to be inserted
     */
    Completable insert(Category category);

    /**
     * Returns a Completable, which on subscription will try to update an existing Category
     * in the repository. If successful, onComplete() will be called.
     *
     * @param   category    category to be updated
     */
    Completable update(Category category);

    /**
     * Returns a Completable, which on subscription will try to delete an existing Category of id.
     * If successful, onComplete() will be called.
     *
     * @param   id          id of the category to be deleted
     */
    Completable delete(String id);
}
