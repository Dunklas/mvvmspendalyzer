package com.dunk.mvvmspendalyzer.data;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.util.Log;

import com.dunk.mvvmspendalyzer.data.model.CategoryDb;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.data.source.CategoryDataSource;
import com.dunk.mvvmspendalyzer.data.source.sql.CategorySQLDataSource;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;

/**
 * Implementation of CategoryRepository.
 * May use one or more CategoryDataSource objects (local db, web, etc) to retrieve data.
 *
 * All db actions are performed on a background thread.
 */
public class CategoryRepositoryImpl implements CategoryRepository {

    private static CategoryRepositoryImpl INSTANCE;

    CategoryDataSource mLocalDataSource;

    private CategoryRepositoryImpl(CategoryDataSource localDataSource) {
        mLocalDataSource = localDataSource;
    }

    public static CategoryRepository getInstance(Context context) {
        if (INSTANCE == null) {
            // TODO: Learn a DI framework and stop doing this..
            CategoryDataSource localDataSource = CategorySQLDataSource.getInstance(context);
            INSTANCE = new CategoryRepositoryImpl(localDataSource);
        }
        return INSTANCE;
    }

    /**
     * Obtain List<CategoryDb> from local data source, convert it
     * to List<Category> and throw RepositoryException if any exception is
     * thrown.
     */
    @Override
    public Flowable<List<Category>> categories() {
        return mLocalDataSource.categories()
                .map(list -> {
                    List<Category> transformedList = new ArrayList<>();
                    for (CategoryDb c : list) {
                        transformedList.add(new Category(
                                c.id(), c.name(), c.type(), c.icon()
                        ));
                    }
                    return transformedList;
                })
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not load categories", e)))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Obtain Maybe<CategoryDb> from local data source, convert it
     * to Maybe<Category> and throw RepositoryException if any exception
     * is thrown.
     */
    @Override
    public Maybe<Category> category(String id) {
        return mLocalDataSource.category(id)
                .map(c -> new Category(c.id(), c.name(), c.type(), c.icon()))
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not load category: " + id, e)))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Insert category to localDataSource.
     */
    @Override
    public Completable insert(Category category) {
        return mLocalDataSource.insert(new CategoryDb(category))
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not insert category: " + category.name(), e)))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Update category in localDataSource.
     */
    @Override
    public Completable update(Category category) {
        return mLocalDataSource.update(new CategoryDb(category))
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not update category: " + category.name(), e)))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Delete category of id in localDataSource.
     */
    @Override
    public Completable delete(String id) {
        return mLocalDataSource.delete(id)
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not delete category: " + id, e)))
                .subscribeOn(Schedulers.io());
    }
}
