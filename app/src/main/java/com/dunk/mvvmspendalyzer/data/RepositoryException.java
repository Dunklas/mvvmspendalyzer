package com.dunk.mvvmspendalyzer.data;

/**
 * Generic exception class for Repository level.
 * To be treated as a fault where recovery is not possible.
 */
public class RepositoryException extends RuntimeException {

    public RepositoryException(String msg) {
        super(msg);
    }

    public RepositoryException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
