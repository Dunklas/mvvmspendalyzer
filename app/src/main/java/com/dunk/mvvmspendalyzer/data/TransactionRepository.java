package com.dunk.mvvmspendalyzer.data;

import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.domain.CategoryType;
import com.dunk.mvvmspendalyzer.domain.Transaction;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * A repository of Transaction objects.
 * Note! All of the methods in this interface may emit a CompositeException containing RepositoryException in onError().
 *
 * Any database or web query performed behind this interface will run on a background thread.
 * Consumer of this interface will need to run .observeOn(AndroidSchedulers.mainThread()) in
 * order to perform actions on mainThread.
 */
public interface TransactionRepository {

    /**
     * Returns a Flowable containing a List of all Transaction-objects in the repository.
     * onNext() will be called each time a change has occurred in the underlying transaction storage.
     * The list is sorted by date.
     */
    Flowable<List<Transaction>> transactions();

    /**
     * Returns a Maybe which may contain a Transaction of id.
     * onSuccess() will be called if a Transaction of id exist, otherwise onComplete() will be called.
     *
     * @param   id      id of queried transaction
     */
    Maybe<Transaction> transaction(String id);

    /**
     * Returns a Completable, which on subscription will try to insert a new Transaction
     * to the repository. If successful, onComplete() will be called.
     *
     * @param   transaction    transaction to be inserted
     */
    Completable insert(Transaction transaction);

    /**
     * Returns a Completable, which on subscription will try to update an existing Transaction
     * in the repository. If successful, onComplete() will be called.
     *
     * @param   transaction    transaction to be updated
     */
    Completable update(Transaction transaction);


    /**
     * Returns a Completable, which on subscription will try to delete an existing Transaction of id.
     * If successful, onComplete() will be called.
     *
     * @param   id          id of the transaction to be deleted
     */
    Completable delete(String id);

    /**
     * Returns a Flowable containing a BigDecimal of total amount of
     * transactions of type, between from and to.
     *
     * @param   from        from date, inclusive
     * @param   to          to date, exclusive
     * @param   type        type of category to include in calculation
     */
    Flowable<BigDecimal> total(Calendar from, Calendar to, CategoryType type);

    /**
     * Returns a Flowable containing a map with total amount, grouped by date, sorted by date.
     *
     * @param   from        from date, inclusive
     * @param   to          to date, exclusive
     * @param   type        type of category to include in calculation
     */
    Flowable<SortedMap<Calendar, BigDecimal>> totalByDate(Calendar from, Calendar to, CategoryType type);

    /**
     * Returns a Flowable containing a map with total amount, grouped by category, sorted by category name.
     *
     * @param   from        from date, inclusive
     * @param   to          to date, exclusive
     * @param   type        type of category to include in calculation
     */
    Flowable<SortedMap<Category, BigDecimal>> totalByCategory(Calendar from, Calendar to, CategoryType type);

}
