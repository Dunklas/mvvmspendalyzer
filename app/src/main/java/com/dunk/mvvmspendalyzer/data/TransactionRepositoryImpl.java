package com.dunk.mvvmspendalyzer.data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.dunk.mvvmspendalyzer.data.model.CategoryDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerCategoryDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerDateDb;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.domain.CategoryType;
import com.dunk.mvvmspendalyzer.domain.Coordinate;
import com.dunk.mvvmspendalyzer.domain.Transaction;
import com.dunk.mvvmspendalyzer.data.model.TransactionDb;
import com.dunk.mvvmspendalyzer.data.source.CategoryDataSource;
import com.dunk.mvvmspendalyzer.data.source.TransactionDataSource;
import com.dunk.mvvmspendalyzer.data.source.sql.CategorySQLDataSource;
import com.dunk.mvvmspendalyzer.data.source.sql.TransactionSQLDataSource;
import com.dunk.mvvmspendalyzer.util.DateUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;

/**
 * Implementation of TransactionRepository.
 * May use one or more TransactionDataSource objects (local db, web, etc) to retrieve data.
 *
 * All db actions are performed on a background thread.
 */
public class TransactionRepositoryImpl implements TransactionRepository {

    private static TransactionRepositoryImpl INSTANCE;

    TransactionDataSource mTransactionSource;

    CategoryRepository mCategoryRepository;

    private Map<String, Category> mCategoryMap = new HashMap<>();

    private TransactionRepositoryImpl(TransactionDataSource transactionSource, CategoryRepository categoryRepository) {
        mTransactionSource = transactionSource;
        mCategoryRepository = categoryRepository;

        setupCategoryMap();
    }

    public static TransactionRepository getInstance(Context context) {
        if (INSTANCE == null) {
            TransactionDataSource localTransactions = TransactionSQLDataSource.getInstance(context);
            CategoryRepository localCategories = CategoryRepositoryImpl.getInstance(context);
            INSTANCE = new TransactionRepositoryImpl(localTransactions, localCategories);
        }
        return INSTANCE;
    }

    /**
     * This class need references to Category-objects in order to construct
     * Transaction objects. Therefore, it maintains a Map<String, Category>,
     * where key is a category id. This method uses flatMapIterable to transform
     * a Flowable<List<Category>> to a Flowable<Category>. Each emitted item is added
     * to mCategoryMap.
     *
     * TODO: Investigate how to stop this subscription. Probably causes a memory leak!
     */
    private void setupCategoryMap() {
        mCategoryRepository.categories()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .flatMapIterable(list -> list)
                .subscribe(c -> mCategoryMap.put(c.id(), c));
    }

    /**
     * Obtain List<TransactionDb> from local data source, convert it
     * to List<Transaction> and throw RepositoryException if any exception is
     * thrown. Category objects are retrieved from mCategoryMap.
     */
    @Override
    public Flowable<List<Transaction>> transactions() {
        return mTransactionSource.transactions()
                .map(list -> {
                    List<Transaction> transformedList = new ArrayList<>();
                    for (TransactionDb t : list) {
                        transformedList.add(new Transaction(
                                t.id(), mCategoryMap.get(t.categoryId()), t.amount(), t.date(), t.note(), t.coordinate())
                        );
                    }
                    return transformedList;
                })
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not load transactions", e)))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Obtain Maybe<TransactionDb> from local data source, convert it
     * to Maybe<Transaction> and throw RepositoryException if any exception
     * is thrown.
     */
    @Override
    public Maybe<Transaction> transaction(String id) {
        return mTransactionSource.transaction(id)
                .map(t -> new Transaction(
                        t.id(), mCategoryMap.get(t.categoryId()), t.amount(), t.date(), t.note(), t.coordinate())
                )
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not load transaction: " + id, e)))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Insert transaction to localDataSource.
     */
    @Override
    public Completable insert(Transaction transaction) {
        return mTransactionSource.insert(new TransactionDb(transaction))
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not insert transaction: " + transaction.id(), e)))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Update transaction in localDataSource.
     */
    @Override
    public Completable update(Transaction transaction) {
        return mTransactionSource.update(new TransactionDb(transaction))
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not update transaction: " + transaction.id(), e)))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Delete transaction of id in localDataSource.
     */
    @Override
    public Completable delete(String id) {
        return mTransactionSource.delete(id)
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not delete transaction: " + id, e)))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Obtain TotalDb from localDataSource, convert it to BigDecimal.
     */
    @Override
    public Flowable<BigDecimal> total(Calendar from, Calendar to, CategoryType type) {
        return mTransactionSource.total(DateUtils.toMidnight(from), DateUtils.toMidnight(to), type)
                .map(totalDb -> totalDb.total())
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not load total")))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Obtain TotalPerDateDb from localDataSource, convert it to SortedMap<Calendar, BigDecimal>.
     * A TreeMap is used in order to have data sorted naturally by date.
     */
    @Override
    public Flowable<SortedMap<Calendar, BigDecimal>> totalByDate(Calendar from, Calendar to, CategoryType type) {
        return mTransactionSource.totalPerDate(DateUtils.toMidnight(from), DateUtils.toMidnight(to), type)
                .map(list -> {
                    SortedMap<Calendar, BigDecimal> result = new TreeMap<>();
                    for (TotalPerDateDb subRes: list) {
                        result.put(subRes.date(), subRes.total());
                    }
                    return result;
                })
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not load total per date")))
                .subscribeOn(Schedulers.io());
    }

    /**
     * Obtain TotalPerCategoryDb from localDataSource, convert it to SortedMap<Category, BigDecimal>.
     * A TreeMap is used in order to have data sorted naturally by category.
     */
    @Override
    public Flowable<SortedMap<Category, BigDecimal>> totalByCategory(Calendar from, Calendar to, CategoryType type) {
        return mTransactionSource.totalPerCategory(DateUtils.toMidnight(from), DateUtils.toMidnight(to), type)
                .map(list -> {
                    SortedMap<Category, BigDecimal> result = new TreeMap<>();
                    for (TotalPerCategoryDb subRes : list) {
                        result.put(mCategoryMap.get(subRes.category()), subRes.total());
                    }
                    return result;
                })
                .doOnError(e -> Exceptions.propagate(new RepositoryException("Could not load total per category")))
                .subscribeOn(Schedulers.io());
    }
}
