package com.dunk.mvvmspendalyzer.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.domain.CategoryType;

import java.util.UUID;

/**
 * Model class used by Room.
 * This class is only used in data layer. In the rest of the application a category is represented
 * by the Category class. There is basically no difference between the classes, but I want to be
 * consistent with how I treat Transaction/TransactionDb.
 */
@Entity(tableName = "categories",
        indices = {@Index(value = {"name"}, unique = true)})
public class CategoryDb {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String mId;

    @NonNull
    @ColumnInfo(name = "name")
    private String mName;

    @NonNull
    @ColumnInfo(name = "type")
    private CategoryType mType;

    @NonNull
    @ColumnInfo(name = "icon")
    private String mIcon;

    @Ignore
    public CategoryDb(@NonNull Category category) {
        this(category.id(), category.name(), category.type(), category.icon());
    }

    public CategoryDb(@NonNull String id, @NonNull String name, @NonNull CategoryType type, @NonNull String icon) {
        mId = id;
        mName = name;
        mType = type;
        mIcon = icon;
    }

    public String id() { return mId; }

    public String name() {
        return mName;
    }

    public CategoryType type() { return mType; }

    public String icon() {
        return mIcon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryDb categoryDb = (CategoryDb) o;

        return mId.equals(categoryDb.mId);
    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }
}
