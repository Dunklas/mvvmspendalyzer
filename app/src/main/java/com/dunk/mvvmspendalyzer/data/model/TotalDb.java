package com.dunk.mvvmspendalyzer.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;

import java.math.BigDecimal;

/**
 * Represent the result of a non-grouped sum operation.
 * Only used in data layer. In other parts of app, this is represented as a BigDecimal.
 */
public class TotalDb {

    @NonNull
    @ColumnInfo(name = "total")
    private BigDecimal mTotal;

    public TotalDb(@NonNull BigDecimal total) {
        mTotal = total;
    }

    public BigDecimal total() {
        return mTotal;
    }
}
