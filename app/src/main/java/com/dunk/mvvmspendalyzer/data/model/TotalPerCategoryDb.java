package com.dunk.mvvmspendalyzer.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;

import java.math.BigDecimal;

/**
 * Represent the result of a sum operation grouped by Category.
 * Only used in data layer. In other parts of app, this is represented as a SortedMap<Category, BigDecimal>.
 */
public class TotalPerCategoryDb {

    @NonNull
    @ColumnInfo(name = "category")
    private String mCategory;

    @NonNull
    @ColumnInfo(name = "total")
    private BigDecimal mTotal;

    public TotalPerCategoryDb(@NonNull String category, @NonNull BigDecimal total) {
        mCategory = category;
        mTotal = total;
    }

    public String category() {
        return mCategory;
    }

    public BigDecimal total() {
        return mTotal;
    }
}
