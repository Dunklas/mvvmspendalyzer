package com.dunk.mvvmspendalyzer.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Represent the result of a sum operation grouped by Calendar.
 * Only used in data layer. In other parts of app, this is represented as a SortedMap<Calendar, BigDecimal>.
 */
public class TotalPerDateDb {

    @NonNull
    @ColumnInfo(name = "date")
    private Calendar mDate;

    @NonNull
    @ColumnInfo(name = "total")
    private BigDecimal mTotal;

    public TotalPerDateDb(@NonNull Calendar date, @NonNull BigDecimal total) {
        mDate = date;
        mTotal = total;
    }

    public Calendar date() {
        return (Calendar) mDate.clone();
    }

    public BigDecimal total() {
        return mTotal;
    }
}
