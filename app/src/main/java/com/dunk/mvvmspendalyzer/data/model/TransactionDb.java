package com.dunk.mvvmspendalyzer.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.dunk.mvvmspendalyzer.domain.Coordinate;
import com.dunk.mvvmspendalyzer.domain.Transaction;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.UUID;

/**
 * Model class used by Room.
 * This class is only used in data layer. In the rest of the application a transaction is represented
 * by the Transaction class. The difference between the classes is that this class only has a id reference
 * to a Category object, while Transaction has a object reference.
 */
@Entity(tableName = "transactions",
        foreignKeys = @ForeignKey(entity = CategoryDb.class,
                                  parentColumns = "id",
                                  childColumns = "category"))
public class TransactionDb {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private String mId;

    @NonNull
    @ColumnInfo(name = "category")
    private String mCategoryId;

    @NonNull
    @ColumnInfo(name = "amount")
    private BigDecimal mAmount;

    @NonNull
    @ColumnInfo(name = "date")
    private Calendar mDate;

    @Nullable
    @ColumnInfo(name = "note")
    private String mNote;

    @Nullable
    @Embedded
    private Coordinate mCoordinate;

    public TransactionDb(@NonNull Transaction transaction) {
        this(transaction.id(), transaction.category().id(), transaction.amount(), transaction.date(),
                transaction.note(), transaction.coordinate());
    }

    public TransactionDb(@NonNull String id, @NonNull String categoryId, @NonNull BigDecimal amount, @NonNull Calendar date,
                         @Nullable String note, @Nullable Coordinate coordinate) {
        mId = id;
        mCategoryId = categoryId;
        mAmount = amount.setScale(2, RoundingMode.CEILING);
        mDate = date;
        mNote = note;
        mCoordinate = coordinate;
    }

    public String id() {
        return mId;
    }

    public String categoryId() {
        return mCategoryId;
    }

    public BigDecimal amount() {
        return mAmount;
    }

    public Calendar date() {
        return (Calendar) mDate.clone();
    }

    public String note() {
        return mNote;
    }

    public Coordinate coordinate() { return mCoordinate; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionDb that = (TransactionDb) o;

        return mId.equals(that.mId);
    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }
}
