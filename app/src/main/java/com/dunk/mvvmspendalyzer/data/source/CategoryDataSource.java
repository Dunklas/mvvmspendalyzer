package com.dunk.mvvmspendalyzer.data.source;

import com.dunk.mvvmspendalyzer.data.model.CategoryDb;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * A source of CategoryDb objects.
 *
 * CategoryRepository implementations uses implementations of this interface
 * to provide data to other parts of the app.
 */
public interface CategoryDataSource {

    /**
     * Will invoke onNext(...) each time a change is
     * detected in source.
     *
     * @return  a stream of List<CategoryDb> objects, sorted by type, name
     */
    Flowable<List<CategoryDb>> categories();

    /**
     * Will invoke onSuccess(...) if category of id exists, otherwise onComplete(...).
     *
     * @param   id  id of queried category
     * @return      a stream which may or may not contain a CategoryDb object
     */
    Maybe<CategoryDb> category(String id);

    /**
     * Will invoke onComplete(...) if insert operation was successful,
     * otherwise onError(...).
     *
     * @param   c   category to be inserted
     * @return      a completable stream
     */
    Completable insert(CategoryDb c);

    /**
     * Will invoke onComplete(...) if update operation was successful,
     * otherwise onError(...).
     *
     * @param   c   category to be updated
     * @return      a completable stream
     */
    Completable update(CategoryDb c);

    /**
     * Will invoke onComplete(...) if delete operation was successful,
     * otherwise onError(...).
     *
     * @param   id  id of category to be deleted
     * @return      a completable stream
     */
    Completable delete(String id);

}
