package com.dunk.mvvmspendalyzer.data.source;

import com.dunk.mvvmspendalyzer.data.model.TotalDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerCategoryDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerDateDb;
import com.dunk.mvvmspendalyzer.data.model.TransactionDb;
import com.dunk.mvvmspendalyzer.domain.CategoryType;

import java.util.Calendar;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * A source of TransactionDb objects.
 *
 * TransactionRepository implementations uses implementations of this interface
 * to provide data to other parts of the app.
 */
public interface TransactionDataSource {

    /**
     * Will invoke onNext(...) each time a change is
     * detected in source.
     *
     * @return  a stream of List<TransactionDb> objects, sorted by date
     */
    Flowable<List<TransactionDb>> transactions();

    /**
     * Will invoke onSuccess(...) if transaction of id exists, otherwise onComplete(...).
     *
     * @param   id  id of queried transaction
     * @return      a stream which may or may not contain a TransactionDb object
     */
    Maybe<TransactionDb> transaction(String id);

    /**
     * Will invoke onComplete(...) if insert operation was successful,
     * otherwise onError(...).
     *
     * @param   t   transaction to be inserted
     * @return      a completable stream
     */
    Completable insert(TransactionDb t);

    /**
     * Will invoke onComplete(...) if update operation was successful,
     * otherwise onError(...).
     *
     * @param   t   transaction to be updated
     * @return      a completable stream
     */
    Completable update(TransactionDb t);

    /**
     * Will invoke onComplete(...) if insert operation was successful,
     * otherwise onError(...).
     *
     * @param   id  id of transaction to be deleted
     * @return      a completable stream
     */
    Completable delete(String id);

    /**
     * Will invoke onNext(...) each time a change is
     * detected in source.
     *
     * @return  a stream of TotalDb objects
     */
    Flowable<TotalDb> total(Calendar from, Calendar to, CategoryType type);

    /**
     * Will invoke onNext(...) each time a change is
     * detected in source.
     *
     * @return  a stream of List<TotalPerCategoryDb> objects, unsorted
     */
    Flowable<List<TotalPerCategoryDb>> totalPerCategory(Calendar from, Calendar to, CategoryType type);

    /**
     * Will invoke onNext(...) each time a change is
     * detected in source.
     *
     * @return  a stream of List<TotalPerDateDb> objects, unsorted
     */
    Flowable<List<TotalPerDateDb>> totalPerDate(Calendar from, Calendar to, CategoryType type);

}
