package com.dunk.mvvmspendalyzer.data.source.fake;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.dunk.mvvmspendalyzer.data.CategoryAlreadyExistException;
import com.dunk.mvvmspendalyzer.data.model.CategoryDb;
import com.dunk.mvvmspendalyzer.data.source.CategoryDataSource;
import com.dunk.mvvmspendalyzer.domain.CategoryType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

public class CategoryFakeDataSource implements CategoryDataSource {

    @Nullable
    private static CategoryFakeDataSource INSTANCE;

    private Map<String, CategoryDb> categories;

    private BehaviorSubject<List<CategoryDb>> subject;

    private CategoryFakeDataSource() {
        categories = new HashMap<>();

        categories.put("1", new CategoryDb("1", "Food and Groceries", CategoryType.EXPENSE, "food-restaurant.png"));
        categories.put("2", new CategoryDb("2", "Rent", CategoryType.EXPENSE, "home-house.png"));
        categories.put("3", new CategoryDb("3", "Utilities", CategoryType.EXPENSE, "home-faucet.png"));
        categories.put("4", new CategoryDb("4", "Gaming", CategoryType.EXPENSE, "entertainment-gamepad.png"));

        subject = BehaviorSubject.create();
    }

    public static CategoryFakeDataSource getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new CategoryFakeDataSource();
        }
        return INSTANCE;
    }

    @Override
    public Flowable<List<CategoryDb>> categories() {

        // Emit current categories on background thread
        Observable.just(1)
                .observeOn(Schedulers.io())
                .doOnNext((i) -> Log.d("Thread", Looper.getMainLooper().equals(Looper.myLooper()) ? "Main" : "Other"))
                .subscribe((i) -> subject.onNext(new ArrayList<>(categories.values())));

        // Return subject as flowable, will emit previously emission upon subscription
        return subject.
                toFlowable(BackpressureStrategy.BUFFER);
    }

    @Override
    public Maybe<CategoryDb> category(String id) {
        return Flowable.fromIterable(categories.values())
                .filter((c) -> c.id().equals(id))
                .singleElement();
    }

    @Override
    public Completable insert(CategoryDb c) {
        return Completable.fromAction(() -> {
           for (CategoryDb cdb : categories.values()) {
               if (cdb.name().equals(c.name())) {
                   throw new CategoryAlreadyExistException("Category " + c.name() + " already exists.");
               }
           }
           categories.put(c.id(), c);
           subject.onNext(new ArrayList<>(categories.values()));
        });
    }

    @Override
    public Completable update(CategoryDb c) {
        return Completable.fromAction(() -> {
            categories.put(c.id(), c);
            subject.onNext(new ArrayList<>(categories.values()));
        });
    }

    @Override
    public Completable delete(String id) {
        return Completable.fromAction(() -> {
           categories.remove(id);
           subject.onNext(new ArrayList<>(categories.values()));
        });
    }
}
