package com.dunk.mvvmspendalyzer.data.source.fake;

import android.content.Context;
import android.support.annotation.Nullable;

import com.dunk.mvvmspendalyzer.data.model.TotalDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerCategoryDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerDateDb;
import com.dunk.mvvmspendalyzer.domain.CategoryType;
import com.dunk.mvvmspendalyzer.domain.Coordinate;
import com.dunk.mvvmspendalyzer.data.model.TransactionDb;
import com.dunk.mvvmspendalyzer.data.source.TransactionDataSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

public class TransactionFakeDataSource implements TransactionDataSource {

    @Nullable
    private static TransactionFakeDataSource INSTANCE;

    private Map<String, TransactionDb> transactions;

    private BehaviorSubject<List<TransactionDb>> subject;

    private TransactionFakeDataSource() {
        transactions = new HashMap<>();
        transactions.put("1", new TransactionDb(
                "1", "1", new BigDecimal(47.50), Calendar.getInstance(), "En utgift", new Coordinate(57.707760, 11.938287))
        );
        transactions.put("2", new TransactionDb(
                "2", "2", new BigDecimal(186.50), Calendar.getInstance(), "En till utgift", new Coordinate(57.707760, 11.938287))
        );

        subject = BehaviorSubject.create();
    }

    public static TransactionFakeDataSource getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new TransactionFakeDataSource();
        }
        return INSTANCE;
    }

    @Override
    public Flowable<List<TransactionDb>> transactions() {
        // Emit current categories on background thread
        Observable.just(1)
                .observeOn(Schedulers.io())
                .subscribe((i) -> subject.onNext(new ArrayList<>(transactions.values())));

        // Return subject as flowable, will emit previously emission upon subscription
        return subject.
                toFlowable(BackpressureStrategy.BUFFER);
    }

    @Override
    public Maybe<TransactionDb> transaction(String id) {
        return Flowable.fromIterable(transactions.values())
                .filter((t) -> t.id().equals(id))
                .singleElement();
    }

    @Override
    public Completable insert(TransactionDb t) {
        return Completable.fromAction(() -> {
            transactions.put(t.id(), t);
            subject.onNext(new ArrayList<>(transactions.values()));
        });
    }

    @Override
    public Completable update(TransactionDb t) {
        return Completable.fromAction(() -> {
            transactions.put(t.id(), t);
            subject.onNext(new ArrayList<>(transactions.values()));
        });
    }

    @Override
    public Completable delete(String tId) {
        return Completable.fromAction(() -> {
            transactions.remove(tId);
            subject.onNext(new ArrayList<>(transactions.values()));
        });
    }

    @Override
    public Flowable<TotalDb> total(Calendar from, Calendar to, CategoryType type) {
        return null;
    }

    @Override
    public Flowable<List<TotalPerCategoryDb>> totalPerCategory(Calendar from, Calendar to, CategoryType type) {
        return null;
        //TODO: Should I implement these?
    }

    @Override
    public Flowable<List<TotalPerDateDb>> totalPerDate(Calendar from, Calendar to, CategoryType type) {
        return null;
    }
}
