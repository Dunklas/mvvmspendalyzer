package com.dunk.mvvmspendalyzer.data.source.sql;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.dunk.mvvmspendalyzer.data.model.CategoryDb;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * DAO class used by Room
 */
@Dao
public interface CategoryDao {

    @Query("SELECT * FROM categories ORDER BY categories.type, categories.name ASC")
    Flowable<List<CategoryDb>> categories();

    @Query("SELECT * FROM categories WHERE id = :categoryId LIMIT 1")
    Maybe<CategoryDb> category(String categoryId);

    //TODO: Detect conflicts and handle them properly
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insert(CategoryDb c);

    @Update
    void update(CategoryDb c);

    @Query("DELETE FROM categories WHERE id = :categoryId")
    void delete(String categoryId);

}
