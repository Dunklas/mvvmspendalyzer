package com.dunk.mvvmspendalyzer.data.source.sql;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.support.annotation.NonNull;
import android.util.Log;

import com.dunk.mvvmspendalyzer.data.CategoryAlreadyExistException;
import com.dunk.mvvmspendalyzer.data.model.CategoryDb;
import com.dunk.mvvmspendalyzer.data.source.CategoryDataSource;
import com.dunk.mvvmspendalyzer.logging.Tags;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.exceptions.Exceptions;

/**
 * Implementation of CategoryDataSource.
 * Uses Room/SQLite.
 */
public class CategorySQLDataSource implements CategoryDataSource {

    private static CategorySQLDataSource INSTANCE;

    private CategoryDao mCategoryDao;

    private CategorySQLDataSource(@NonNull CategoryDao categoryDao) {
        mCategoryDao = categoryDao;
    }

    public static CategorySQLDataSource getInstance(Context context) {
        if (INSTANCE == null) {
            SpendalyzerDatabase db = SpendalyzerDatabase.getInstance(context.getApplicationContext());
            INSTANCE = new CategorySQLDataSource(db.categoryDao());
        }
        return INSTANCE;
    }

    @Override
    public Flowable<List<CategoryDb>> categories() {
        return mCategoryDao.categories()
                .doOnNext(l -> Log.i(Tags.LOG_DATA, "List<CategoryDb> emitted in onNext()"));
    }

    @Override
    public Maybe<CategoryDb> category(String id) {
        return mCategoryDao.category(id)
                .doOnSuccess(c -> Log.i(Tags.LOG_DATA, "Maybe<CategoryDb> emitted in onSuccess()"));
    }

    /**
     * Detects SQLiteConstraintException and rethrow as CategoryAlreadyExistException.
     * Other exceptions are propagated as usual.
     */
    @Override
    public Completable insert(CategoryDb c) {
        return Completable.fromAction(() -> mCategoryDao.insert(c))
                .doOnError(e -> {
                    if (e instanceof SQLiteConstraintException) {
                        Exceptions.propagate(new CategoryAlreadyExistException("Category " + c.name() + " already exists.", e));
                    } else {
                        Exceptions.propagate(e);
                    }
                })
                .doOnComplete(() -> Log.i(Tags.LOG_DATA, "CategoryDb insert operation emitted onComplete()"));
    }

    @Override
    public Completable update(CategoryDb c) {
        return Completable.fromAction(() -> mCategoryDao.update(c))
                .doOnComplete(() -> Log.i(Tags.LOG_DATA, "CategoryDb update operation emitted onComplete()"));
    }

    @Override
    public Completable delete(String id) {
        return Completable.fromAction(() -> mCategoryDao.delete(id))
                .doOnComplete(() -> Log.i(Tags.LOG_DATA, "CategoryDb delete operation emitted onComplete()"));
    }
}
