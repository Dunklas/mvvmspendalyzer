package com.dunk.mvvmspendalyzer.data.source.sql;

import android.arch.persistence.room.TypeConverter;

import com.dunk.mvvmspendalyzer.data.model.CategoryDb;
import com.dunk.mvvmspendalyzer.domain.CategoryType;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Contains TypeConverters used by Room to transform
 * data during read and write operations.
 */
public class Converters {

    /**
     * Converts a String to CategoryType (used during read).
     */
    @TypeConverter
    public static CategoryType cTypeFromString(String value) {
        return CategoryType.valueOf(value);
    }

    /**
     * Converts a CategoryType to String (used during write).
     */
    @TypeConverter
    public static String cTypeToString(CategoryType type) {
        return type.toString();
    }

    /**
     * Converts a Long to BigDecimal (used during read).
     */
    @TypeConverter
    public static BigDecimal bigDecimalFromLong(Long value) {
        if (value == null) {
            return new BigDecimal(0);
        } else {
            return new BigDecimal(value).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
        }
    }

    /**
     * Converts a BigDecimal to Long (used during write).
     * TODO: Investigate what the implications would be if using localized number of digits after decimal as scale
     */
    @TypeConverter
    public static Long bigDecimalToLong(BigDecimal bigDecimal) {
        return bigDecimal.multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP).longValue();
    }

    /**
     * Converts a Long to Calendar (used during read).
     */
    @TypeConverter
    public static Calendar calendarFromTimetamp(Long value) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(value);
        return cal;
    }

    /**
     * Converts a Calendar to Long (used during write).
     */
    @TypeConverter
    public static Long calendarToTimestamp(Calendar cal) {
        return cal.getTimeInMillis();
    }
}
