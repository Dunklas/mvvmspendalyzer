package com.dunk.mvvmspendalyzer.data.source.sql;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.dunk.mvvmspendalyzer.data.model.CategoryDb;
import com.dunk.mvvmspendalyzer.data.model.TransactionDb;

/**
 * Abstract database class used (and extended) by Room.
 * Singleton, in order to prevent multiple database connections.
 */
@Database(entities = {CategoryDb.class, TransactionDb.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class SpendalyzerDatabase extends RoomDatabase {

    private static SpendalyzerDatabase INSTANCE;

    abstract CategoryDao categoryDao();

    abstract TransactionDao transactionDao();

    private static final Object sLock = new Object();

    public static SpendalyzerDatabase getInstance(Context context) {
        synchronized (sLock) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        SpendalyzerDatabase.class, "Spendalyzer.db")
                        .build();
            }
            return INSTANCE;
        }
    }
}
