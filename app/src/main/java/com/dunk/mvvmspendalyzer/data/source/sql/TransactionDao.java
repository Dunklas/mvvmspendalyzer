package com.dunk.mvvmspendalyzer.data.source.sql;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.dunk.mvvmspendalyzer.data.model.TotalDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerCategoryDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerDateDb;
import com.dunk.mvvmspendalyzer.data.model.TransactionDb;
import com.dunk.mvvmspendalyzer.domain.CategoryType;

import java.util.Calendar;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * DAO class used by Room
 */
@Dao
public interface TransactionDao {

    @Query("SELECT * FROM transactions ORDER BY transactions.date DESC")
    Flowable<List<TransactionDb>> transactions();

    @Query("SELECT * FROM transactions WHERE id = :transactionId LIMIT 1")
    Maybe<TransactionDb> transaction(String transactionId);

    //TODO: Detect conflicts and handle them properly
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insert(TransactionDb t);

    @Update
    void update(TransactionDb t);

    @Query("DELETE FROM transactions WHERE id = :transactionId")
    void delete(String transactionId);

    @Query("SELECT sum(amount) AS total" +
        " FROM transactions INNER JOIN categories ON transactions.category = categories.id" +
        " WHERE categories.type = :type AND transactions.date >= :from AND transactions.date < :to")
    Flowable<TotalDb> total(Calendar from, Calendar to, CategoryType type);

    @Query("SELECT transactions.date AS date, sum(amount) AS total" +
        " FROM transactions INNER JOIN categories ON transactions.category = categories.id" +
        " WHERE categories.type = :type AND transactions.date >= :from AND transactions.date < :to" +
        " GROUP BY transactions.date")
    Flowable<List<TotalPerDateDb>> totalPerDate(Calendar from, Calendar to, CategoryType type);

    @Query("SELECT categories.id AS category, sum(amount) AS total" +
        " FROM transactions INNER JOIN categories ON transactions.category = categories.id" +
        " WHERE categories.type = :type AND transactions.date >= :from AND transactions.date < :to" +
        " GROUP BY categories.id")
    Flowable<List<TotalPerCategoryDb>> totalPerCategory(Calendar from, Calendar to, CategoryType type);
}
