package com.dunk.mvvmspendalyzer.data.source.sql;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.dunk.mvvmspendalyzer.data.model.TotalDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerCategoryDb;
import com.dunk.mvvmspendalyzer.data.model.TotalPerDateDb;
import com.dunk.mvvmspendalyzer.data.model.TransactionDb;
import com.dunk.mvvmspendalyzer.data.source.TransactionDataSource;
import com.dunk.mvvmspendalyzer.domain.CategoryType;
import com.dunk.mvvmspendalyzer.logging.Tags;

import java.util.Calendar;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Implementation of TransactionDataSource.
 * Uses Room/SQLite.
 */
public class TransactionSQLDataSource implements TransactionDataSource {

    private static TransactionSQLDataSource INSTANCE;

    private TransactionDao mTransactionDao;

    private TransactionSQLDataSource(@NonNull TransactionDao transactionDao) {
        mTransactionDao = transactionDao;
    }

    public static TransactionSQLDataSource getInstance(Context context) {
        if (INSTANCE == null) {
            SpendalyzerDatabase db = SpendalyzerDatabase.getInstance(context.getApplicationContext());
            INSTANCE = new TransactionSQLDataSource(db.transactionDao());
        }
        return INSTANCE;
    }

    @Override
    public Flowable<List<TransactionDb>> transactions() {
        return mTransactionDao.transactions()
                .doOnNext(l -> Log.i(Tags.LOG_DATA, "List<TransactionDb> emitted in onNext()"));
    }

    @Override
    public Maybe<TransactionDb> transaction(String id) {
        return mTransactionDao.transaction(id)
                .doOnSuccess(t -> Log.i(Tags.LOG_DATA, "Maybe<TransactionDb> emitted in onSuccess()"));
    }

    @Override
    public Completable insert(TransactionDb t) {
        return Completable.fromAction(() -> mTransactionDao.insert(t))
                .doOnComplete(() -> Log.i(Tags.LOG_DATA, "TransactionDb insert operation emitted onComplete()"));
    }

    @Override
    public Completable update(TransactionDb t) {
        return Completable.fromAction(() -> mTransactionDao.update(t))
                .doOnComplete(() -> Log.i(Tags.LOG_DATA, "TransactionDb update operation emitted onComplete()"));
    }

    @Override
    public Completable delete(String transactionId) {
        return Completable.fromAction(() -> mTransactionDao.delete(transactionId))
                .doOnComplete(() -> Log.i(Tags.LOG_DATA, "TransactionDb delete operation emitted onComplete()"));
    }

    @Override
    public Flowable<TotalDb> total(Calendar from, Calendar to, CategoryType type) {
        return mTransactionDao.total(from, to, type)
                .doOnNext(t -> Log.i(Tags.LOG_DATA, "TotalDb emitted in onNext()"));
    }

    @Override
    public Flowable<List<TotalPerCategoryDb>> totalPerCategory(Calendar from, Calendar to, CategoryType type) {
        return mTransactionDao.totalPerCategory(from, to, type)
                .doOnNext(tpc -> Log.i(Tags.LOG_DATA, "TotalPerCategoryDb emitted in onNext()"));
    }

    @Override
    public Flowable<List<TotalPerDateDb>> totalPerDate(Calendar from, Calendar to, CategoryType type) {
        return mTransactionDao.totalPerDate(from, to, type)
                .doOnNext(tpd -> Log.i(Tags.LOG_DATA, "TotalPerDateDb emitted in onNext()"));
    }
}
