package com.dunk.mvvmspendalyzer.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Nullable;

// TODO: Implement version id for all Serializable classes

/**
 * Represents a time period which a user may see an overview of his/her transactions.
 * The word "Accounting" is used because it would be kind of logical in a
 * business context, even though this application most likely is used to keep
 * track of private economy.
 *
 * This class is immutable. Therefore, clones of internal Calendar objects are
 * returned by public methods.
 */
public class AccountingPeriod implements Serializable {

    private Calendar mFrom;
    private Calendar mTo;

    // Package private, since this class should only be instantiated by an AccountingPeriodManager
    AccountingPeriod(Calendar from, Calendar to) {
        mFrom = (Calendar) from.clone();
        mTo = (Calendar) to.clone();
    }

    /**
     * Returns a Calendar object representing the first
     * date in this AccountingPeriod. The date is inclusive, so that
     * 2017-06-01 00:00:00:00 would be returned if this AccountingPeriod
     * starts on 2017-06-01.
     */
    public Calendar from() {
        return (Calendar) mFrom.clone();
    }

    /**
     * Returns a Calendar object representing the last
     * date in this AccountingPeriod. The date is exclusive, so that
     * 2017-07-01 00:00:00:00 would be returned if this AccountingPeriod
     * ends on 2017-06-30.
     */
    public Calendar to() {
        return (Calendar) mTo.clone();
    }

    /**
     * Returns a List<Calendar> containing a Calendar object
     * for each date in this AccountingPeriod.
     */
    public List<Calendar> range() {
        List<Calendar> range = new ArrayList<>();
        Calendar tmp = (Calendar) mFrom.clone();

        while (tmp.before(mTo)) {
            range.add((Calendar)tmp.clone());
            tmp.add(Calendar.DAY_OF_MONTH, 1);
        }

        return range;
    }
}
