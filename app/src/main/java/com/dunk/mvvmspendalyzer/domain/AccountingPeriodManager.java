package com.dunk.mvvmspendalyzer.domain;

import com.dunk.mvvmspendalyzer.util.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This class is responsible for instantiating and provide
 * a number of AccountingPeriod-objects.
 *
 * From current date, the AccountingPeriod objects should cover
 * one year back in time, and one month into the future.
 * The duration of each AccountingPeriod object is decided by the consumer
 * of this class, by invoking either weekly(Calendar now), biweekly(Calendar now), or
 * monthly(Calendar now, int firstOfMonth) to instantiate this class.
 */
public class AccountingPeriodManager {

    /* The Calendar object which "seeds" this object. Should be the current date. */
    private Calendar mToday;

    private List<AccountingPeriod> mPeriods = new ArrayList<>();

    /* Index of the AccountingPeriod which include mToday */
    private int mStartIndex;

    /* Represents the date to be considered as the first date of a month */
    private Integer mFirstDayOfMonth;

    /* Represents duration of each AccountingPeriod */
    private AccountingPeriodType mPeriod;

    private AccountingPeriodManager(Calendar today, AccountingPeriodType period, Integer firstDayOfMonth) {

        mToday = DateUtils.toMidnight(today);
        mPeriod = period;

        switch (mPeriod) {
            case WEEKLY:
                calculateWeeklyPeriods();
                break;
            case BIWEEKLY:
                calculateBiWeeklyPeriods();
                break;
            case MONTHLY:
                mFirstDayOfMonth = firstDayOfMonth;
                calculateMonthlyPeriods();
                break;
            default:
                throw new IllegalArgumentException("Illegal period");
        }
    }

    /**
     * Returns an AccountingPeriodManager where each AccountingPeriod
     * cover one week.
     *
     * @param   now     A Calendar object to be used as "seed" for this AccountingPeriodManager
     */
    public static AccountingPeriodManager weekly(Calendar now) {
        return new AccountingPeriodManager(now, AccountingPeriodType.WEEKLY, null);
    }

    /**
     * Returns an AccountingPeriodManager where each AccountingPeriod
     * cover two weeks.
     *
     * @param   now     A Calendar object to be used as "seed" for this AccountingPeriodManager
     */
    public static AccountingPeriodManager biweekly(Calendar now) {
        return new AccountingPeriodManager(now, AccountingPeriodType.BIWEEKLY, null);
    }

    /**
     * Returns an AccountingPeriodManager where each AccountingPeriod
     * cover one month.
     *
     * @param   now                 A Calendar object to be used as "seed" for this AccountingPeriodManager
     * @param   firstDayOfMonth     An integer representing the date to be considered as first of each month, must be 0-28
     */
    public static AccountingPeriodManager monthly(Calendar now, int firstDayOfMonth) {

        if (firstDayOfMonth <= 0 || firstDayOfMonth > 28) {
            throw new IllegalArgumentException("Invalid firstDayOfMonth: " + firstDayOfMonth +
                    ". Must be an integer between 1-28.");
        }

        return new AccountingPeriodManager(now, AccountingPeriodType.MONTHLY, firstDayOfMonth);
    }

    /**
     * Returns the AccountingPeriod of index.
     */
    public AccountingPeriod period(int index) {
        return mPeriods.get(index);
    }

    /**
     * Returns the total number of AccountingPeriod objects managed by this AccountingPeriodManager.
     */
    public int numberPeriods() {
        return mPeriods.size();
    }

    /**
     * Returns index of the AccountingPeriod which cover the Calendar object
     * used to seed this AccountingPeriodManager.
     */
    public int today() {
        return mStartIndex;
    }

    private void calculateWeeklyPeriods() {
        Calendar temp = (Calendar) mToday.clone();
        Calendar lastWeek = (Calendar) mToday.clone();
        temp.add(Calendar.YEAR, -1);
        lastWeek.add(Calendar.WEEK_OF_YEAR, 4);

        while (temp.before(lastWeek) || DateUtils.isSameWeek(temp, lastWeek)) {
            Calendar start = DateUtils.firstDateOfWeek(temp);
            Calendar end = (Calendar) start.clone();
            end.add(Calendar.WEEK_OF_YEAR, 1);

            AccountingPeriod tempPeriod = new AccountingPeriod(start, end);
            mPeriods.add(tempPeriod);

            if (DateUtils.isSameWeek(temp, mToday)) {
                mStartIndex = mPeriods.indexOf(tempPeriod);
            }

            temp.add(Calendar.WEEK_OF_YEAR, 1);
        }
    }

    private void calculateBiWeeklyPeriods() {
        Calendar temp = (Calendar) mToday.clone();
        Calendar lastWeek = (Calendar) mToday.clone();
        temp.add(Calendar.YEAR, -1);
        lastWeek.add(Calendar.WEEK_OF_YEAR, 4);

        while (temp.before(lastWeek) || DateUtils.isSameWeek(temp, lastWeek)) {
            Calendar start = DateUtils.firstDateOfBiWeekly(temp);
            Calendar end = (Calendar) start.clone();
            end.add(Calendar.WEEK_OF_YEAR, 2);

            AccountingPeriod tempPeriod = new AccountingPeriod(start, end);
            mPeriods.add(tempPeriod);

            if (DateUtils.isSameWeek(temp, mToday)) {
                mStartIndex = mPeriods.indexOf(tempPeriod);
            }

            temp.add(Calendar.WEEK_OF_YEAR, 2);
        }
    }

    private void calculateMonthlyPeriods() {
        Calendar temp = (Calendar) mToday.clone();
        Calendar lastMonth = (Calendar) mToday.clone();
        temp.add(Calendar.YEAR, -1);
        lastMonth.add(Calendar.MONTH, 1);

        while (temp.before(lastMonth) || DateUtils.isSameMonth(temp, lastMonth)) {
            Calendar start = DateUtils.firstDateOfMonth(mFirstDayOfMonth, temp);
            Calendar end = (Calendar) start.clone();
            end.add(Calendar.MONTH, 1);

            AccountingPeriod tempPeriod = new AccountingPeriod(start, end);
            mPeriods.add(tempPeriod);

            if (DateUtils.isSameMonth(temp, mToday)) {
                mStartIndex = mPeriods.indexOf(tempPeriod);
            }

            temp.add(Calendar.MONTH, 1);
        }
    }
}
