package com.dunk.mvvmspendalyzer.domain;

/**
 * Represents the different types of AccountingPeriod-durations
 * that may be used.
 */
public enum AccountingPeriodType {

    WEEKLY,
    BIWEEKLY,
    MONTHLY

}
