package com.dunk.mvvmspendalyzer.domain;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.UUID;

/**
 * Represents a user defined category which Transaction objects
 * may be related to.
 *
 * Objects of this class are immutable.
 */
public class Category implements Serializable, Comparable<Category> {

    public static int NAME_MAX_LENGTH = 15;

    @NonNull
    private String mId;

    @NonNull
    private String mName;

    @NonNull
    private CategoryType mType;

    @NonNull
    private String mIcon;

    /**
     * Returns a Category instance.
     *
     * @param id    id of this Category. If id is null, a new id will be generated
     * @param name  name of this Category, e.g. "Food and Groceries"
     * @param type  type of this Category, e.g. CategoryType.INCOME
     * @param icon  (file name of) icon to be related with this Category
     */
    public Category(@Nullable String id, @NonNull String name, @NonNull CategoryType type, @NonNull String icon) {
        if (Category.isValid(id, name, type, icon)) {
            mId = id == null ? UUID.randomUUID().toString() : id;
            mName = name;
            mType = type;
            mIcon = icon;
        } else {
            // TODO: Notify what argument that was invalid?
            throw new IllegalArgumentException("Invalid arguments");
        }
    }

    public static boolean isValid(@Nullable String id, @NonNull String name, @NonNull CategoryType type, @NonNull String icon) {
        if (name == null || type == null || icon == null) return false;

        if (name.length() > NAME_MAX_LENGTH) {
            return false;
        }

        return true;
    }

    public String id() { return mId; }

    public String name() {
        return mName;
    }

    public CategoryType type() {
        return mType;
    }

    public String icon() {
        return mIcon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category that = (Category) o;

        return mId.equals(that.mId);
    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }

    @Override
    public int compareTo(@NonNull Category category) {
        return this.name().compareTo(category.name());
    }
}
