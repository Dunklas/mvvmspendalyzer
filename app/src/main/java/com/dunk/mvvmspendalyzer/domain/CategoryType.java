package com.dunk.mvvmspendalyzer.domain;

/**
 * Represents different type of categories.
 */
public enum CategoryType {

    INCOME,
    EXPENSE

}
