package com.dunk.mvvmspendalyzer.domain;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Represents a coordinate.
 *
 * Objects of this class are immutable.
 */
public class Coordinate implements Serializable {

    private Double latitude;
    private Double longitude;

    public Coordinate(@NonNull Double latitude, @NonNull Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double latitude() {
        return latitude;
    }

    public Double longitude() {
        return longitude;
    }
}
