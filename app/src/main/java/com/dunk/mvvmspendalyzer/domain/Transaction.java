package com.dunk.mvvmspendalyzer.domain;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.dunk.mvvmspendalyzer.util.DateUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.UUID;

/**
 * Represents a monetary transaction.
 * The amount of this transaction is always positive. However
 * the transaction may represent an expense, but it's decided
 * by the category (or rather, its CategoryType) which this Transaction
 * is related to.
 *
 * Objects of this class are immutable.
 */
public class Transaction {

    // Lowest legal value of mAmount
    public static int AMOUNT_MIN = 0; // Must be larger than AMOUNT_MIN

    // Max length of the integer part of mAmount, e.g. 120247.99 is legal, 1202471 is illegal
    public static int AMOUNT_INT_LENGTH = 6;

    // TODO: Use localized number of digits after decimal, 2 is not used everywhere...
    // Max length of digits allowed after decimal, e.g. 47.99 is legal, 47.347 is illegal
    public static int AMOUNT_MAX_DECIMALS = 2;

    // Max length of mNote
    public static int NOTE_MAX_LENGTH = 80;

    @NonNull
    private String mId;

    @NonNull
    private Category mCategory;

    @NonNull
    private BigDecimal mAmount;

    @NonNull
    private Calendar mDate;

    @Nullable
    private String mNote;

    @Nullable
    private Coordinate mCoordinate;

    /**
     * Returns a Transaction instance.
     *
     * @param id            id of this Transaction. If id is null, a new id will be generated
     * @param category      category which this Transaction is related to
     * @param amount        the monetary value of this Transaction, should always be positive, even though this Transaction may represent an expense
     * @param date          the date when this Transaction occurred
     * @param note          an optional note describing this Transaction
     * @param coordinate    the location where this Transaction occurred
     */
    public Transaction(@Nullable String id, @NonNull Category category, @NonNull BigDecimal amount, @NonNull Calendar date,
                       @Nullable String note, @Nullable Coordinate coordinate) {
        if (Transaction.isValid(id, category, amount, date, note, coordinate)) {
            mId = id == null ? UUID.randomUUID().toString() : id;
            mCategory = category;
            mAmount = amount.setScale(AMOUNT_MAX_DECIMALS, BigDecimal.ROUND_HALF_UP);
            mDate = DateUtils.toMidnight(date);
            mNote = note;
            mCoordinate = coordinate;
        } else {
            throw new IllegalArgumentException("Invalid arguments");
        }
    }

    public static boolean isValid(@Nullable String id, @NonNull Category category, @NonNull BigDecimal amount, @NonNull Calendar date,
                           @Nullable String note, @Nullable Coordinate coordinate) {
        if (category == null || amount == null || date == null) return false;

        if (amount.compareTo(new BigDecimal(AMOUNT_MIN)) <= 0) {
            return false;
        }

        // Precision is total amount of digits, scale is amount of digits after decimal
        amount = amount.stripTrailingZeros();
        if (amount.precision() - amount.scale() > AMOUNT_INT_LENGTH) {
            return false;
        }

        if (note != null) {
            if (note.length() > NOTE_MAX_LENGTH) {
                return false;
            }
        }

        return true;
    }

    @NonNull
    public String id() {
        return mId;
    }

    @NonNull
    public Category category() { return mCategory; }

    @NonNull
    public BigDecimal amount() {
        return mAmount;
    }

    @NonNull
    public Calendar date() {
        return (Calendar) mDate.clone();
    }

    @Nullable
    public String note() {
        return mNote;
    }

    @Nullable
    public Coordinate coordinate() { return mCoordinate; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        return mId.equals(that.mId);
    }

    @Override
    public int hashCode() {
        return mId.hashCode();
    }
}
