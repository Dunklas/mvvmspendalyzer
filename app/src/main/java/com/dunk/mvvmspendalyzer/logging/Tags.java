package com.dunk.mvvmspendalyzer.logging;

/**
 * Class to keep TAG constants used while logging.
 * Using aspects instead of class names, inspired by
 * this blog post: http://blog.bangbits.com/2016/09/best-practice-android-logging.html
 */
public class Tags {

    /**
     * Logging tag used for UI related events.
     */
    public static final String LOG_UI = "UI";

    /**
     * Logging tag used for data related events.
     */
    public static final String LOG_DATA = "DATA";

    /**
     * Logging tag used for business logic and other app related events,
     * not covered by other tags.
     */
    public static final String LOG_APP = "APP";

    private Tags() {}
}
