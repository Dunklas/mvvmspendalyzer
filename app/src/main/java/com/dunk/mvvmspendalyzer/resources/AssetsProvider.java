package com.dunk.mvvmspendalyzer.resources;

import android.graphics.drawable.Drawable;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Provides files from Assets directory.
 */
public interface AssetsProvider {

    /**
     * Returns a Observable<String[]> containing the file names of all icons
     * in assets directory.
     */
    Observable<String[]> iconNames();

    /**
     * Returns a Observable<Drawable> for the icon with file name equal to name argument.
     * If no icon is found, a 'default' one is returned.
     */
    Observable<Drawable> icon(String name);
}
