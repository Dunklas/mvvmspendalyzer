package com.dunk.mvvmspendalyzer.resources;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.util.Log;

import com.dunk.mvvmspendalyzer.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

/**
 * Implementation of AssetsProvider.
 */
public class AssetsProviderImpl implements AssetsProvider {

    private static AssetsProviderImpl INSTANCE;

    private static final String ICON_FOLDER = "icons";

    private Context mContext;

    private AssetsProviderImpl(Context ctx) {
        mContext = ctx.getApplicationContext();
    }

    public static AssetsProvider getInstance(Context ctx) {
        if (INSTANCE == null) {
            INSTANCE = new AssetsProviderImpl(ctx);
        }
        return INSTANCE;
    }

    @Override
    public Observable<String[]> iconNames() {

        return Observable.fromCallable(() -> {
            AssetManager assets = mContext.getAssets();
            String[] iconNames = assets.list(ICON_FOLDER);
            return iconNames;
        }).subscribeOn(Schedulers.io());

    }

    @Override
    public Observable<Drawable> icon(final String name) {
        return Observable.fromCallable(() -> {
            AssetManager assets = mContext.getAssets();
            String path = ICON_FOLDER + "/" + name;
            InputStream input = assets.open(path);
            return Drawable.createFromStream(input, name);
        }).onErrorReturn(e -> mContext.getDrawable(R.drawable.ic_image)) // If IOException occurs, a 'default' drawable will be returned
          .subscribeOn(Schedulers.io());
    }
}
