package com.dunk.mvvmspendalyzer.resources;

import com.dunk.mvvmspendalyzer.domain.AccountingPeriodType;

/**
 * Provides settings/preferences.
 */
public interface SettingsProvider {

    /**
     * Returns an integer representing the date to be used as first of each month.
     */
    int firstDayOfMonth();

    /**
     * Returns an AccountPeriodType which indicates the duration of each AccountPeriod to be used.
     */
    AccountingPeriodType dashboardPeriod();

}
