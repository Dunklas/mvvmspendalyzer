package com.dunk.mvvmspendalyzer.resources;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.domain.AccountingPeriodType;

/**
 * Implementation of SettingsProvider, using PreferenceManager (SharedPreferences).
 * Note that default values specified in preferences.xml must be initialized early
 * during launch of application. Therefore, this is done in SplashActivity.
 */
public class SettingsProviderImpl implements SettingsProvider {

    private static SettingsProviderImpl INSTANCE;

    private SharedPreferences mSharedPref;
    private Context mContext;

    public static SettingsProvider getInstance(Context ctx) {
        if (INSTANCE == null) {
            INSTANCE = new SettingsProviderImpl(ctx);
        }
        return INSTANCE;
    }

    private SettingsProviderImpl(Context ctx) {
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        mContext = ctx.getApplicationContext();
    }

    @Override
    public int firstDayOfMonth() {
        String key = mContext.getString(R.string.settings_first_of_month_key);
        return Integer.valueOf(mSharedPref.getString(key, ""));
    }

    @Override
    public AccountingPeriodType dashboardPeriod() {
        String key = mContext.getString(R.string.settings_dashboard_period_key);
        return AccountingPeriodType.valueOf(mSharedPref.getString(key, ""));
    }
}
