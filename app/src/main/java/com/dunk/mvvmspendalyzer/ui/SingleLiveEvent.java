package com.dunk.mvvmspendalyzer.ui;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Lifecycle-aware class to be used for events, such as navigation, or snackbar messages.
 *
 * Only one observer will be notified of changes.
 * observe(...), setValue(...) and call() must be called from the main thread (since LiveData must rely on synchronous calls to avoid
 * that non-active lifecycle owners are notified of change).
 *
 * If setValue() or call() is invoked during e.g. a configuration change, the update
 * will be made once there is an active observer.
 */
public class SingleLiveEvent<T> extends MutableLiveData<T> {

    private static final String TAG = "SingleLiveEvent";

    private final AtomicBoolean mPending = new AtomicBoolean(false);

    @MainThread
    public void observe(LifecycleOwner owner, final Observer<T> observer) {

        if (hasActiveObservers()) {
            Log.w(TAG, "Multiple observers registered but only one will be notified of changes");
        }

        super.observe(owner, new Observer<T>() {
            @Override
            public void onChanged(@Nullable T t) {
                if (mPending.compareAndSet(true, false)) {
                    observer.onChanged(t);
                }
            }
        });
    }

    @MainThread
    public void setValue(@Nullable T t) {
        mPending.set(true);
        super.setValue(t);
    }

    /**
     * Used for cases where T is void
     */
    @MainThread
    public void call() {
        setValue(null);
    }
}
