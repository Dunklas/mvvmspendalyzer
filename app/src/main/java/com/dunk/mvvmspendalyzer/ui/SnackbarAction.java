package com.dunk.mvvmspendalyzer.ui;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A SingleLiveEvent used for snackbar messages with an action.
 * Uses a custom observer.
 */
public class SnackbarAction extends SingleLiveEvent<Void> {

    @StringRes
    private int message;

    @StringRes
    private int actionLabel;

    private Runnable action;

    public SnackbarAction(@StringRes int message, @StringRes int actionLabel, @NonNull Runnable action) {
        checkNotNull(action);
        this.message = message;
        this.actionLabel = actionLabel;
        this.action = action;
    }

    @MainThread
    public void observe(LifecycleOwner owner, final SnackbarActionObserver observer) {
        super.observe(owner, new Observer<Void>() {
            @Override
            public void onChanged(Void v) {
                observer.onNewAction(message, actionLabel, action);
            }
        });
    }

    public interface SnackbarActionObserver {

        void onNewAction(@StringRes int snackbarMessageResourceId, @StringRes int actionLabel, Runnable action);

    }
}
