package com.dunk.mvvmspendalyzer.ui;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

/**
 * A SingleLiveEvent used for snackbar messages.
 * Uses a custom observer.
 */
public class SnackbarMessage extends SingleLiveEvent<Integer> {

    @MainThread
    public void observe(LifecycleOwner owner, final SnackbarMessageObserver observer) {
        super.observe(owner, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer resource) {
                if (resource == null) {
                    return;
                }
                observer.onNewMessage(resource);
            }
        });
    }

    public interface SnackbarMessageObserver {

        void onNewMessage(@StringRes int snackbarMessageResourceId);

    }
}
