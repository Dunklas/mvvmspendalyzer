package com.dunk.mvvmspendalyzer.ui.about;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dunk.mvvmspendalyzer.R;

/**
 * Fragment for an 'About'-page where e.g. attributions are made.
 */
public class AboutFragment extends Fragment {

    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_frag, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        // Required to have hyperlinks in a TextView...
        TextView attribution = view.findViewById(R.id.about_attribution);
        attribution.setMovementMethod(LinkMovementMethod.getInstance());

        setupToolbar();
    }

    private void setupToolbar() {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.about_title);
        }
    }
}
