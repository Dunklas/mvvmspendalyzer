package com.dunk.mvvmspendalyzer.ui.addeditcategory;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;
import com.dunk.mvvmspendalyzer.ui.common.base.BaseActivity;
import com.dunk.mvvmspendalyzer.ui.pickicon.PickIconActivity;
import com.dunk.mvvmspendalyzer.viewmodels.AddEditCategoryViewModel;

/**
 * Activity where a Category may be created or modified.
 *
 * If this Activity is started with an Intent including the key
 * EXTRA_CATEGORY_ID, an existing Category will be displayed.
 */
public class AddEditCategoryActivity extends BaseActivity implements AddEditCategoryNavigator {

    public static final String EXTRA_CATEGORY_ID = "com.dunk.mvvmspendalyzer.ui.addeditcategory.category_id";

    private static final int REQUEST_ICON = 0;

    private AddEditCategoryViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = AddEditCategoryActivity.obtainViewModel(this);

        mViewModel.saveCategoryEvent.observe(this, (v) -> onCategorySaved());
        mViewModel.selectIconEvent.observe(this, (v) -> onSelectIcon());
    }

    @Override
    public void onCategorySaved() {
        finish();
    }

    @Override
    public void onSelectIcon() {
        Intent intent = new Intent(this, PickIconActivity.class);
        startActivityForResult(intent, REQUEST_ICON);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_ICON) {
            String iconName = data.getStringExtra(PickIconActivity.EXTRA_ICON_NAME);
            mViewModel.setIcon(iconName);
        }
    }

    @Override
    protected Fragment fragment() {
        String categoryId = getIntent().getStringExtra(EXTRA_CATEGORY_ID);
        return AddEditCategoryFragment.newInstance(categoryId);
    }

    @Override
    protected @StringRes int title() {
        String categoryId = getIntent().getStringExtra(EXTRA_CATEGORY_ID);
        return categoryId == null ? R.string.category_add_title : R.string.category_edit_title;
    }


    static AddEditCategoryViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(AddEditCategoryViewModel.class);
    }
}
