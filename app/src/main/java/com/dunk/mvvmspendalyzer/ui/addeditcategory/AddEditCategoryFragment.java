package com.dunk.mvvmspendalyzer.ui.addeditcategory;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.databinding.AddEditCategoryBinding;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;
import com.dunk.mvvmspendalyzer.util.SnackbarUtils;
import com.dunk.mvvmspendalyzer.viewmodels.AddEditCategoryViewModel;

/**
 * Fragment where a Category may be created or modified.
 */
public class AddEditCategoryFragment extends Fragment {

    private static final String ARGUMENT_CATEGORY_ID = "CATEGORY_ID";

    private AddEditCategoryViewModel mViewModel;

    private AddEditCategoryBinding mViewDataBinding;

    private TextInputLayout mNameLayout;
    private ViewGroup mLayout;

    /**
     * Returns an instance of AddEditCategoryFragment.
     *
     * @param categoryId    id of the category to be displayed, may be null (if fragment should display a new/blank category)
     */
    public static AddEditCategoryFragment newInstance(String categoryId) {
        Bundle args = new Bundle();
        args.putString(ARGUMENT_CATEGORY_ID, categoryId);
        AddEditCategoryFragment fragment = new AddEditCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = AddEditCategoryActivity.obtainViewModel(getActivity());
        mViewModel.start(getArguments().getString(ARGUMENT_CATEGORY_ID));

        // This observable will be changed if the form has become valid or invalid
        mViewModel.formValid.observe(getActivity(), (b) -> getActivity().invalidateOptionsMenu());

        /* This observable will be changed if we tried to save this category, but detected
         * that a category with the same name already exist */
        mViewModel.categoryAlreadyExistEvent.observe(AddEditCategoryFragment.this,
                (SnackbarMessage.SnackbarMessageObserver) (msg) -> showCategoryAlreadyExistError(msg));

        // This observable will be changed if an error should be displayed
        mViewModel.showErrorEvent.observe(AddEditCategoryFragment.this,
                (SnackbarMessage.SnackbarMessageObserver) (msg) -> showError(msg));

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.category_addedit_frag, container, false);
        if (mViewDataBinding == null) {
            mViewDataBinding = AddEditCategoryBinding.bind(root);
        }
        mViewDataBinding.setViewmodel(mViewModel);

        return mViewDataBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mNameLayout = view.findViewById(R.id.category_addedit_name_layout);
        mLayout = view.findViewById(R.id.category_addedit_layout);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.category_addedit, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.nav_category_addedit_save).setEnabled(mViewModel.formValid.getValue());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_category_addedit_save:
                mViewModel.saveCategory();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showError(@StringRes int msg) {
        SnackbarUtils.message(mLayout, msg);
    }

    private void showCategoryAlreadyExistError(@StringRes int msg) {
        mNameLayout.setErrorEnabled(true);
        mNameLayout.setError(getString(msg));
    }
}
