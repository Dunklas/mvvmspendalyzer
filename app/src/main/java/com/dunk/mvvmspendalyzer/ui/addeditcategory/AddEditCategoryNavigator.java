package com.dunk.mvvmspendalyzer.ui.addeditcategory;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface AddEditCategoryNavigator {

    /**
     * Called when user want to save this Category.
     */
    void onCategorySaved();

    /**
     * Called when user want to select an icon for this Category.
     */
    void onSelectIcon();
}
