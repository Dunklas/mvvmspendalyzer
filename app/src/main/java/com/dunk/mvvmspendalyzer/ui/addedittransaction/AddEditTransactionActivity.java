package com.dunk.mvvmspendalyzer.ui.addedittransaction;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.domain.Coordinate;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;
import com.dunk.mvvmspendalyzer.ui.common.base.BaseActivity;
import com.dunk.mvvmspendalyzer.ui.pickcategory.PickCategoryActivity;
import com.dunk.mvvmspendalyzer.ui.pickdate.PickDateFragment;
import com.dunk.mvvmspendalyzer.ui.picklocation.PickLocationActivity;
import com.dunk.mvvmspendalyzer.viewmodels.AddEditTransactionViewModel;

import java.util.Calendar;

/**
 * Activity where a Transaction may be created or modified.
 *
 * If this Activity is started with an Intent including the key
 * EXTRA_TRANSACTION_ID, an existing Category will be displayed.
 */
public class AddEditTransactionActivity extends BaseActivity
        implements AddEditTransactionNavigator, PickDateFragment.OnDatePickedListener {

    public static final String EXTRA_TRANSACTION_ID = "com.dunk.mvvmspendalyzer.ui.addedittransaction.transaction_id";

    private static final int REQUEST_CATEGORY = 0;
    private static final int REQUEST_LOCATION = 1;

    private AddEditTransactionViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = AddEditTransactionActivity.obtainViewModel(this);

        mViewModel.saveTransactionEvent.observe(this, (v) -> onTransactionSaved());
        mViewModel.selectCategoryEvent.observe(this, (v) -> onSelectCategory());
        mViewModel.selectDateEvent.observe(this, (date) -> onSelectDate(date));
        mViewModel.selectLocationEvent.observe(this, (location) -> onSelectLocation(location));
    }

    @Override
    protected Fragment fragment() {
        String transactionId = getIntent().getStringExtra(EXTRA_TRANSACTION_ID);
        return AddEditTransactionFragment.newInstance(transactionId);
    }

    @Override
    protected int title() {
        String transactionId = getIntent().getStringExtra(EXTRA_TRANSACTION_ID);
        return transactionId == null ? R.string.transaction_add_title : R.string.transaction_edit_title;
    }

    static AddEditTransactionViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(AddEditTransactionViewModel.class);
    }

    @Override
    public void onTransactionSaved() {
        finish();
    }

    @Override
    public void onSelectCategory() {
        Intent intent = new Intent(this, PickCategoryActivity.class);
        startActivityForResult(intent, REQUEST_CATEGORY);
    }

    @Override
    public void onSelectDate(Calendar currentDate) {
        DialogFragment fragment = PickDateFragment.newInstance(currentDate);
        fragment.show(getSupportFragmentManager(), "DatePicker");
    }

    @Override
    public void onSelectLocation(Coordinate currentCoordinate) {
        Intent intent = new Intent(this, PickLocationActivity.class);
        intent.putExtra(PickLocationActivity.EXTRA_LOCATION, currentCoordinate);
        startActivityForResult(intent, REQUEST_LOCATION);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CATEGORY) {
            Category category = (Category) data.getSerializableExtra(PickCategoryActivity.EXTRA_CATEGORY);
            mViewModel.setCategory(category);
        } else if (requestCode == REQUEST_LOCATION) {
            Coordinate location = (Coordinate) data.getSerializableExtra(PickLocationActivity.EXTRA_LOCATION);
            mViewModel.setLocation(location);
        }
    }

    @Override
    public void onDatePicked(Calendar date) {
        mViewModel.setDate(date);
    }
}
