package com.dunk.mvvmspendalyzer.ui.addedittransaction;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.domain.Coordinate;
import com.dunk.mvvmspendalyzer.databinding.AddEditTransactionBinding;
import com.dunk.mvvmspendalyzer.domain.Transaction;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;
import com.dunk.mvvmspendalyzer.util.SnackbarUtils;
import com.dunk.mvvmspendalyzer.viewmodels.AddEditTransactionViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Fragment where a Transaction may be created or modified.
 */
public class AddEditTransactionFragment extends Fragment {

    private static final String ARGUMENT_TRANSACTION_ID = "TRANSACTION_ID";

    private AddEditTransactionViewModel mViewModel;

    private AddEditTransactionBinding mViewDataBinding;

    private Button mLocationAddButton;
    private Button mLocationChangeButton;
    private Button mLocationRemoveButton;
    private FrameLayout mMapContainer;

    private EditText mAmountField;
    private ViewGroup mLayout;

    /**
     * Returns an instance of AddEditTransactionFragment.
     *
     * @param transactionId    id of the transaction to be displayed, may be null (if fragment should display a new/blank transaction)
     */
    public static AddEditTransactionFragment newInstance(String transactionId) {
        Bundle args = new Bundle();
        args.putString(ARGUMENT_TRANSACTION_ID, transactionId);
        AddEditTransactionFragment fragment = new AddEditTransactionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = AddEditTransactionActivity.obtainViewModel(getActivity());
        mViewModel.start(getArguments().getString(ARGUMENT_TRANSACTION_ID));

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.transaction_addedit_frag, container, false);
        if (mViewDataBinding == null) {
            mViewDataBinding = AddEditTransactionBinding.bind(root);
        }
        mViewDataBinding.setViewmodel(mViewModel);

        return mViewDataBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLocationAddButton = view.findViewById(R.id.transaction_addedit_location_add);
        mLocationRemoveButton = view.findViewById(R.id.transaction_addedit_location_remove);
        mLocationChangeButton = view.findViewById(R.id.transaction_addedit_location_change);
        mMapContainer = view.findViewById(R.id.transaction_addedit_map_container);
        mAmountField = view.findViewById(R.id.transaction_addedit_amount_field);
        mLayout = view.findViewById(R.id.transaction_addedit_layout);

        setupObservers();
        setupValidationRestrictions();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.transaction_addedit, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.nav_transaction_addedit_save).setEnabled(mViewModel.formValid.getValue());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_transaction_addedit_save:
                mViewModel.saveTransaction();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Set restrictions for amountField (for number of integers before and after decimal point)
    private void setupValidationRestrictions() {
        mAmountField.setFilters(new InputFilter[] {
                new DecimalDigitsInputFilter(Transaction.AMOUNT_INT_LENGTH, Transaction.AMOUNT_MAX_DECIMALS)
        });
    }

    private void setupObservers() {

        // This observable will be changed if the form has become valid or invalid
        mViewModel.formValid.observe(AddEditTransactionFragment.this, (b) -> getActivity().invalidateOptionsMenu());

        // This observable will be changed if an error should be displayed
        mViewModel.showErrorEvent.observe(AddEditTransactionFragment.this,
                (SnackbarMessage.SnackbarMessageObserver) (msg) -> showError(msg));

        /* This observable will be changed if a new location has been set (or removed) for this transaction.
         * Different buttons should be displayed, depending on if a location has been set or not. */
        mViewModel.location.observe(AddEditTransactionFragment.this, (loc) -> {
           if (loc == null) {
               toggleMap(false, loc);
               mLocationAddButton.setVisibility(View.VISIBLE);
               mLocationRemoveButton.setVisibility(View.GONE);
               mLocationChangeButton.setVisibility(View.GONE);
           } else {
               toggleMap(true, loc);
               mLocationRemoveButton.setVisibility(View.VISIBLE);
               mLocationChangeButton.setVisibility(View.VISIBLE);
               mLocationAddButton.setVisibility(View.GONE);
           }
        });
    }

    private void toggleMap(boolean show, @Nullable Coordinate location) {
        // Find mapFragment, if any
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.transaction_addedit_map_container);

        if (mapFragment != null && !show) {
            // If we have a mapFragment but it shouldn't be displayed...Remove it.
            getChildFragmentManager()
                    .beginTransaction()
                    .remove(mapFragment)
                    .commit();
        } else if (show) {
            if (mapFragment == null) {
                // If we don't have a mapFragment and we should display one...Add one.
                mapFragment = SupportMapFragment.newInstance();
                getChildFragmentManager()
                        .beginTransaction()
                        .add(R.id.transaction_addedit_map_container, mapFragment)
                        .commit();
            }

            //Obtain a GoogleMap instance
            mapFragment.getMapAsync(map -> {
                if (location != null) {
                    // Set location on the map, if we have one.
                    LatLng latLng = new LatLng(location.latitude(), location.longitude());
                    map.clear();
                    map.addMarker(new MarkerOptions().position(latLng));
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                    map.getUiSettings().setAllGesturesEnabled(false);
                }
            });
        }

        // Show or hide mapContainer
        int visibility = show ? View.VISIBLE : View.GONE;
        mMapContainer.setVisibility(visibility);
    }

    private void showError(@StringRes int msg) {
        SnackbarUtils.message(mLayout, msg);
    }
}
