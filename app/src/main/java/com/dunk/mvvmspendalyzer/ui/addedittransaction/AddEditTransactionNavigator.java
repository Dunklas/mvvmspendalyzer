package com.dunk.mvvmspendalyzer.ui.addedittransaction;

import com.dunk.mvvmspendalyzer.domain.Coordinate;

import java.util.Calendar;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface AddEditTransactionNavigator {

    /**
     * Called when user want to save this Transaction.
     */
    void onTransactionSaved();

    /**
     * Called when user want to select a Category for this Transaction.
     */
    void onSelectCategory();

    /**
     * Called when user want to select a date for this Transaction.
     *
     * @param currentDate   the date currently set for this Transaction
     */
    void onSelectDate(Calendar currentDate);

    /**
     * Called when user want to select a new location for this Transaction.
     *
     * @param currentCoordinate the location currently set for this Transaction
     */
    void onSelectLocation(Coordinate currentCoordinate);
}
