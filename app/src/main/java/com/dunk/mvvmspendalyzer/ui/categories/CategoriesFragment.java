package com.dunk.mvvmspendalyzer.ui.categories;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.databinding.CategoriesBinding;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;
import com.dunk.mvvmspendalyzer.util.SnackbarUtils;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;
import com.dunk.mvvmspendalyzer.ui.addeditcategory.AddEditCategoryActivity;
import com.dunk.mvvmspendalyzer.ui.common.category.CategoryAdapter;
import com.dunk.mvvmspendalyzer.viewmodels.CategoriesViewModel;

/**
 * Fragment where a list of Category objects are displayed.
 */
public class CategoriesFragment extends Fragment implements CategoriesNavigator {

    private CategoriesViewModel mViewModel;

    private RecyclerView mRecyclerView;
    private CategoryAdapter mAdapter;

    private CategoriesBinding mViewDataBinding;

    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = CategoriesFragment.obtainViewModel(getActivity());
        mViewModel.start();

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.categories_frag, container, false);
        if (mViewDataBinding == null) {
            mViewDataBinding = CategoriesBinding.bind(root);
        }
        mViewDataBinding.setViewmodel(mViewModel);
        return mViewDataBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setupToolbar();
        setupRecyclerView(view);
        setupObservers();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.categories, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_categories_new:
                mViewModel.newCategoryClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.categories_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new CategoryAdapter(mRecyclerView);
        mAdapter.setOnCategoryClickedListener((category) -> mViewModel.existingCategoryClicked(category.id()));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupObservers() {
        // This observable will be changed when user click an existing category
        mViewModel.clickExistingCategoryEvent.observe(this, (categoryId) -> onExistingCategory(categoryId));

        // This observable will be changed when user want to create a new category
        mViewModel.clickNewCategoryEvent.observe(this, (v) -> onNewCategory());

        // This observable will be changed when an error should be displayed
        mViewModel.showError.observe(this,
                (SnackbarMessage.SnackbarMessageObserver) (msg) -> showError(msg));
    }

    //TODO: Should Activitys decide title?
    private void setupToolbar() {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.categories_title);
        }
    }
    
    public static CategoriesViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(CategoriesViewModel.class);
    }

    @Override
    public void onNewCategory() {
        Intent intent = new Intent(getActivity(), AddEditCategoryActivity.class);
        startActivity(intent);
    }

    @Override
    public void onExistingCategory(String categoryId) {
        Intent intent = new Intent(getActivity(), AddEditCategoryActivity.class);
        intent.putExtra(AddEditCategoryActivity.EXTRA_CATEGORY_ID, categoryId);
        startActivity(intent);
    }

    private void showError(@StringRes int errorMsg) {
        SnackbarUtils.message(mRecyclerView, errorMsg);
    }
}
