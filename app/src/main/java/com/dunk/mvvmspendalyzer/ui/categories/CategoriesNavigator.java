package com.dunk.mvvmspendalyzer.ui.categories;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface CategoriesNavigator {

    /**
     * Called when user want to create a new Category.
     */
    void onNewCategory();

    /**
     * Called when user want to open an existing Category.
     *
     * @param categoryId    id of the Category to be opened
     */
    void onExistingCategory(String categoryId);
}
