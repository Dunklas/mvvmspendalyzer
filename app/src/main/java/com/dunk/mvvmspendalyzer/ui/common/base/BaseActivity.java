package com.dunk.mvvmspendalyzer.ui.common.base;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.util.ActivityUtils;
import com.dunk.mvvmspendalyzer.util.TintUtils;

/**
 * A base activity extended by all Activity-classes, except for MainActivity
 */
public abstract class BaseActivity extends AppCompatActivity {

    // Should return a fragment to be displayed in this activity
    protected abstract Fragment fragment();

    // Should return a title to be set in the app bar
    protected abstract @StringRes int title();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_act);

        Fragment fragment = findOrCreateFragment();
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(),
                fragment, R.id.base_content_frame);

        setupToolbar();
    }

    // Set correct color for all icons in the AppBar
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        TintUtils.tintMenu(this, menu, R.color.colorIconDark);
        return true;
    }

    private Fragment findOrCreateFragment() {
        Fragment fragment = getSupportFragmentManager()
                .findFragmentById(R.id.base_content_frame);
        if (fragment == null) {
            fragment = fragment();
        }
        return fragment;
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle();
    }

    private void setTitle() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title());
        }
    }
}
