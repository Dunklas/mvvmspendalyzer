package com.dunk.mvvmspendalyzer.ui.common.category;

import android.os.Handler;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.databinding.CategoryListItemBinding;
import com.dunk.mvvmspendalyzer.domain.CategoryType;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Adapter which displays Category objects in a RecyclerView.
 *
 * This adapter expects to get a list of Category objects sorted by
 * CategoryType (in order to display a header for each type).
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder>
    implements OnCategoryClickedListener {

    private RecyclerView mRecyclerView;
    private List<Category> mCategories = new ArrayList<>();

    private OnCategoryClickedListener mListener;

    private Queue<List<Category>> pendingUpdates = new ArrayDeque<>();

    public CategoryAdapter(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater =
                LayoutInflater.from(parent.getContext());
        CategoryListItemBinding itemBinding =
                CategoryListItemBinding.inflate(inflater, parent, false);
        return new CategoryViewHolder(itemBinding, this);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        Category c = mCategories.get(position);
        holder.bind(c);
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    /**
     * Updates the internal list of Category objects of this adapter.
     * Internally uses DiffUtil to calculate differences between the new
     * and old list, and then notifies this adapter for each change. The DiffUtil calculation
     * is performed on a background thread and any concurrent invocations of this
     * method will be placed in a queue, which is processed in order.
     *
     * @param   newCategories   a list of Category objects, sorted by CategoryType
     */
    public void update(List<Category> newCategories) {
        pendingUpdates.add(newCategories);
        if (pendingUpdates.size() > 1) {
            return;
        }
        updateItemsInternal(newCategories);
    }

    private void updateItemsInternal(final List<Category> newCategories) {
        final List<Category> oldCategories = new ArrayList<>(mCategories);

        final Handler handler = new Handler();
        new Thread(() -> {
            // Calculate difference between new and old list on background thread
            final DiffUtil.DiffResult diffResult =
                    DiffUtil.calculateDiff(new CategoryDiffCallback(oldCategories, newCategories));
            handler.post(() -> {

                pendingUpdates.remove(newCategories);

                // Notify adapter on main thread
                applyDiffResult(newCategories, diffResult);
                updateHeaders(newCategories);

                if (pendingUpdates.size() > 0) {
                    // If any pending updates, process next one
                    updateItemsInternal(pendingUpdates.peek());
                }
            });
        }).start();
    }

    private void applyDiffResult(List<Category> newCategories, DiffUtil.DiffResult diffResult) {
        diffResult.dispatchUpdatesTo(this);
        mCategories.clear();
        mCategories.addAll(newCategories);
    }

    private void updateHeaders(List<Category> newCategories) {
        // Remove old headers
        if (mRecyclerView.getItemDecorationCount() > 0) {
            mRecyclerView.removeItemDecorationAt(0);
        }

        // Get index of first income and first expense category in the list
        Category firstIncomeCategory  = null;
        Category firstExpenseCategory = null;
        for (int i = newCategories.size()-1; i >= 0; i--) {
            if (newCategories.get(i).type() == CategoryType.INCOME) {
                firstIncomeCategory = newCategories.get(i);
            } else if (newCategories.get(i).type() == CategoryType.EXPENSE) {
                firstExpenseCategory = newCategories.get(i);
            }
        }

        // Add new headers
        mRecyclerView.addItemDecoration(new CategoryHeaderDecoration(mRecyclerView.getContext(),
                firstExpenseCategory, firstIncomeCategory), 0);
        mRecyclerView.invalidateItemDecorations();
    }

    /**
     * Sets a OnCategoryClickedListener to this adapter.
     * Only one listener is supported. If a previous one is set, it will be overwritten.
     *
     * @param   listener    OnCategoryClickedListener to be notified of clicks
     */
    public void setOnCategoryClickedListener(OnCategoryClickedListener listener) {
        mListener = listener;
    }

    @Override
    public void onCategoryClicked(Category category) {
        if (mListener != null) {
            mListener.onCategoryClicked(category);
        }
    }
}
