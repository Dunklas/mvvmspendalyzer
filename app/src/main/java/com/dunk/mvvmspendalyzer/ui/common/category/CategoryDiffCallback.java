package com.dunk.mvvmspendalyzer.ui.common.category;

import android.support.v7.util.DiffUtil;

import com.dunk.mvvmspendalyzer.domain.Category;

import java.util.List;

/**
 * Calculates the difference between two lists of Category objects.
 * See: https://developer.android.com/reference/android/support/v7/util/DiffUtil.html
 */
public class CategoryDiffCallback extends DiffUtil.Callback {

    private List<Category> oldCategories;
    private List<Category> newCategories;

    public CategoryDiffCallback(List<Category> oldCategories, List<Category> newCategories) {
        this.oldCategories = oldCategories;
        this.newCategories = newCategories;
    }

    @Override
    public int getOldListSize() {
        return oldCategories.size();
    }

    @Override
    public int getNewListSize() {
        return newCategories.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldCategories.get(oldItemPosition)
                .equals(newCategories.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Category oldC = oldCategories.get(oldItemPosition);
        Category newC = newCategories.get(newItemPosition);

        return oldC.name().equals(newC.name()) &&
                oldC.icon().equals(newC.icon());
    }
}
