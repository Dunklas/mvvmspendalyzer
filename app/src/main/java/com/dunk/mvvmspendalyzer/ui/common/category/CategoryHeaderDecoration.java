package com.dunk.mvvmspendalyzer.ui.common.category;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.domain.Category;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of ItemDecoration to display headers
 * in a RecyclerView of Category objects.
 */
public class CategoryHeaderDecoration extends RecyclerView.ItemDecoration {

    // First expense category in the RecyclerView
    private final Category mFirstExpense;

    // First income category in the RecyclerView
    private final Category mFirstIncome;

    // View objects for the actual headers
    private View mExpenseHeader;
    private View mIncomeHeader;

    public CategoryHeaderDecoration(Context ctx, Category firstExpense, Category firstIncome) {
       mFirstExpense = firstExpense;
       mFirstIncome = firstIncome;

       if (mFirstExpense != null) {
           mExpenseHeader = getHeaderView(ctx, R.string.category_adapter_expense);
       }

       if (mFirstIncome != null) {
           mIncomeHeader = getHeaderView(ctx, R.string.category_adapter_income);
       }
    }

    @Override
    public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        // Loop every item in the RecyclerView
        for (int i = 0; i < parent.getChildCount(); i++) {

            // Obtain category and position for this item
            View child = parent.getChildAt(i);
            Category c = ((CategoryViewHolder)parent.getChildViewHolder(child)).category();
            int position = parent.getChildAdapterPosition(child);

            // If header should be rendered, do it.
            if (position != RecyclerView.NO_POSITION && hasHeader(c)) {

                View headerView;
                if (c.equals(mFirstExpense)) {
                    headerView = mExpenseHeader;
                } else {
                    headerView = mIncomeHeader;
                }

                canvas.save();
                canvas.translate(0, child.getY() - headerView.getHeight());
                headerView.draw(canvas);
                canvas.restore();
            }
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        // Obtain position and category of this item
        int position = parent.getChildLayoutPosition(view);
        Category c = ((CategoryViewHolder)parent.getChildViewHolder(view)).category();

        // If header should be added for this item, measure it and set offsets
        if (position != RecyclerView.NO_POSITION && hasHeader(c)) {

            View headerView;
            if (c.equals(mFirstExpense)) {
                headerView = mExpenseHeader;
            } else {
                headerView = mIncomeHeader;
            }

            measureHeaderView(headerView, parent);
            outRect.top = headerView.getHeight();
        }
    }

    // Measures view
    protected void measureHeaderView(View view, ViewGroup parent) {
        if (view.getLayoutParams() == null) {
            view.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        final DisplayMetrics displayMetrics = parent.getContext().getResources().getDisplayMetrics();

        int widthSpec = View.MeasureSpec.makeMeasureSpec(displayMetrics.widthPixels, View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(displayMetrics.heightPixels, View.MeasureSpec.EXACTLY);

        int childWidth = ViewGroup.getChildMeasureSpec(widthSpec,
                parent.getPaddingLeft() + parent.getPaddingRight(), view.getLayoutParams().width);
        int childHeight = ViewGroup.getChildMeasureSpec(heightSpec,
                parent.getPaddingTop() + parent.getPaddingBottom(), view.getLayoutParams().height);

        view.measure(childWidth, childHeight);

        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
    }

    // Returns true if category should have a header
    private boolean hasHeader(Category category) {
        return category != null & (category.equals(mFirstExpense) || category.equals(mFirstIncome));
    }

    // Returns a View to be used as a header
    private View getHeaderView(Context ctx, @StringRes int label) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View header = inflater.inflate(R.layout.list_header, null);
        TextView headerTextView = header.findViewById(R.id.list_header_label);
        headerTextView.setText(label);
        return header;
    }
}
