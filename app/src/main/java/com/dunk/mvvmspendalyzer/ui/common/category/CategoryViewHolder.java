package com.dunk.mvvmspendalyzer.ui.common.category;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.databinding.CategoryListItemBinding;

class CategoryViewHolder extends RecyclerView.ViewHolder
    implements View.OnClickListener {

    private final CategoryListItemBinding mBinding;

    private Category mCategory;

    private OnCategoryClickedListener mListener;

    CategoryViewHolder(CategoryListItemBinding binding, OnCategoryClickedListener listener) {
        super(binding.getRoot());
        mBinding = binding;

        itemView.setOnClickListener(this);
        mListener = listener;
    }

    public void bind(Category c) {
        mCategory = c;
        mBinding.setCategory(c);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.onCategoryClicked(mCategory);
        }
    }

    public Category category() {
        return mCategory;
    }
}