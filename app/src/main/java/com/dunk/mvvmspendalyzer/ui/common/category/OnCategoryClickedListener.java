package com.dunk.mvvmspendalyzer.ui.common.category;

import com.dunk.mvvmspendalyzer.domain.Category;

public interface OnCategoryClickedListener {

    void onCategoryClicked(Category category);

}
