package com.dunk.mvvmspendalyzer.ui.common.icon;

import android.os.Handler;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.dunk.mvvmspendalyzer.databinding.IconListItemBinding;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * Adapter which displays icons in a RecyclerView.
 * Each icon is represented by a String of its file name.
 *
 * To actually display icons (Drawable) data binding is used.
 */
public class IconAdapter extends RecyclerView.Adapter<IconViewHolder>
    implements OnIconClickedListener {

    private Deque<List<String>> pendingUpdates = new ArrayDeque<>();

    private List<String> mIcons = new ArrayList<>();
    private OnIconClickedListener mListener;

    @Override
    public IconViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater =
                LayoutInflater.from(parent.getContext());
        IconListItemBinding itemBinding =
                IconListItemBinding.inflate(inflater, parent, false);
        return new IconViewHolder(itemBinding, this);
    }

    @Override
    public void onBindViewHolder(IconViewHolder holder, int position) {
        String icon = mIcons.get(position);
        holder.bind(icon);
    }

    @Override
    public int getItemCount() {
        return mIcons.size();
    }

    /**
     * Sets a OnIconClickedListener to this adapter.
     * Only one listener is supported. If a previous one is set, it will be overwritten.
     *
     * @param   listener    OnIconClickedListener to be notified of clicks
     */
    public void setOnIconClickedListener(OnIconClickedListener listener) {
        mListener = listener;
    }

    /**
     * Updates the internal list of String objects of this adapter.
     * Internally uses DiffUtil to calculate differences between the new
     * and old list, and then notifies this adapter for each change. The DiffUtil calculation
     * is performed on a background thread and any concurrent invocations of this
     * method will be placed in a queue.
     *
     * Note! Only the latest item added to the queue will be processed. The reason for this is
     * that this adapter is used to filter icons by file name. If a new filter criteria has been entered,
     * we don't need to process all the previous ones. Only the latest one is important.
     *
     * @param   newIcons   a list of Category objects, sorted by CategoryType
     */
    public void update(final List<String> newIcons) {
        pendingUpdates.push(newIcons);
        if (pendingUpdates.size() > 1) {
            return;
        }
        updateItemsInternal(newIcons);
    }

    private void updateItemsInternal(final List<String> newIcons) {
        final List<String> oldIcons = new ArrayList<>(mIcons);

        final Handler handler = new Handler();
        new Thread(() -> {
            // Calculate difference between new and old list on background thread
            final DiffUtil.DiffResult diffResult =
                    DiffUtil.calculateDiff(new IconDiffCallback(oldIcons, newIcons));
            handler.post(() -> {

                pendingUpdates.remove(newIcons);
                // Notify adapter on main thread
                applyDiffResult(newIcons, diffResult);

                if (pendingUpdates.size() > 0) {
                    // Obtain and process latest update, clear the rest!
                    List<String> latest = pendingUpdates.pop();
                    pendingUpdates.clear();
                    updateItemsInternal(latest);
                }
            });
        }).start();
    }

    private void applyDiffResult(List<String> newIcons, DiffUtil.DiffResult diffResult) {
        diffResult.dispatchUpdatesTo(this);
        mIcons.clear();
        mIcons.addAll(newIcons);
    }

    @Override
    public void onIconClicked(String icon) {
        if (mListener != null) {
            mListener.onIconClicked(icon);
        }
    }
}
