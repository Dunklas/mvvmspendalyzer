package com.dunk.mvvmspendalyzer.ui.common.icon;

import android.support.v7.util.DiffUtil;

import java.util.List;

/**
 * Calculates the difference between two lists of String objects.
 * See: https://developer.android.com/reference/android/support/v7/util/DiffUtil.html
 */
public class IconDiffCallback extends DiffUtil.Callback {

    private List<String> oldList;
    private List<String> newList;

    public IconDiffCallback(List<String> oldList, List<String> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }
}
