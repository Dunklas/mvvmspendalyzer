package com.dunk.mvvmspendalyzer.ui.common.icon;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dunk.mvvmspendalyzer.databinding.IconListItemBinding;

public class IconViewHolder extends RecyclerView.ViewHolder
    implements View.OnClickListener {

    private final IconListItemBinding mBinding;
    private String mIcon;

    private OnIconClickedListener mListener;

    public IconViewHolder(IconListItemBinding binding, OnIconClickedListener listener) {
        super(binding.getRoot());
        mBinding = binding;

        itemView.setOnClickListener(this);
        mListener = listener;
    }

    public void bind(String icon) {
        mIcon = icon;
        mBinding.setIcon(icon);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.onIconClicked(mIcon);
        }
    }
}
