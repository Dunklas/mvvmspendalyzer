package com.dunk.mvvmspendalyzer.ui.common.icon;

public interface OnIconClickedListener {

    void onIconClicked(String icon);

}
