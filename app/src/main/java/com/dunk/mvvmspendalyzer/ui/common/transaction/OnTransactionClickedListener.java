package com.dunk.mvvmspendalyzer.ui.common.transaction;

public interface OnTransactionClickedListener {

    void onTransactionClicked(String transactionId);

}
