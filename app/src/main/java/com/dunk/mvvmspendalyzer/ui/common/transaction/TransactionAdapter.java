package com.dunk.mvvmspendalyzer.ui.common.transaction;

import android.os.Handler;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.dunk.mvvmspendalyzer.domain.Transaction;
import com.dunk.mvvmspendalyzer.databinding.TransactionListItemBinding;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Queue;

/**
 * Adapter which displays Transaction objects in a RecyclerView.
 */
public class TransactionAdapter extends RecyclerView.Adapter<TransactionViewHolder>
    implements OnTransactionClickedListener {

    private List<Transaction> mTransactions = new ArrayList<>();

    private OnTransactionClickedListener mListener;

    private Queue<List<Transaction>> pendingUpdates = new ArrayDeque<>();

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater =
                LayoutInflater.from(parent.getContext());
        TransactionListItemBinding itemBinding =
                TransactionListItemBinding.inflate(inflater, parent, false);
        return new TransactionViewHolder(itemBinding, this);
    }

    @Override
    public void onBindViewHolder(TransactionViewHolder holder, int position) {
        Transaction t = mTransactions.get(position);
        holder.bind(t);
    }

    @Override
    public int getItemCount() {
        return mTransactions.size();
    }


    /**
     * Updates the internal list of Transaction objects of this adapter.
     * Internally uses DiffUtil to calculate differences between the new
     * and old list, and then notifies this adapter for each change. The DiffUtil calculation
     * is performed on a background thread and any concurrent invocations of this
     * method will be placed in a queue, which is processed in order.
     *
     * @param   newTransactions   a list of Category objects, sorted by CategoryType
     */
    public void update(List<Transaction> newTransactions) {
        pendingUpdates.add(newTransactions);
        if (pendingUpdates.size() > 1) {
            return;
        }
        updateItemsInternal(newTransactions);
    }

    private void updateItemsInternal(final List<Transaction> newTransactions) {
        final List<Transaction> oldTransactions = new ArrayList<>(mTransactions);

        final Handler handler = new Handler();
        new Thread(() -> {
            // Calculate difference between new and old list on background thread
            final DiffUtil.DiffResult diffResult =
                    DiffUtil.calculateDiff(new TransactionDiffCallback(oldTransactions, newTransactions));
            handler.post(() -> {

                pendingUpdates.remove();

                // Notify adapter on main thread
                applyDiffResult(newTransactions, diffResult);
                if (pendingUpdates.size() > 0) {
                    // If any pending updates, process next one
                    updateItemsInternal(pendingUpdates.peek());
                }
            });
        }).start();
    }

    private void applyDiffResult(List<Transaction> newTransactions, DiffUtil.DiffResult diffResult) {
        diffResult.dispatchUpdatesTo(this);
        mTransactions.clear();
        mTransactions.addAll(newTransactions);
    }

    /**
     * Sets a OnTransactionClickedListener to this adapter.
     * Only one listener is supported. If a previous one is set, it will be overwritten.
     *
     * @param   listener    OnTransactionClickedListener to be notified of clicks
     */
    public void setOnTransactionClickedListener(OnTransactionClickedListener listener) {
        mListener = listener;
    }

    @Override
    public void onTransactionClicked(String transactionId) {
        if (mListener != null) {
            mListener.onTransactionClicked(transactionId);
        }
    }
}
