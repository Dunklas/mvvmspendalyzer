package com.dunk.mvvmspendalyzer.ui.common.transaction;

import android.support.v7.util.DiffUtil;

import com.dunk.mvvmspendalyzer.domain.Transaction;

import java.util.List;

/**
 * Calculates the difference between two lists of Transaction objects.
 * See: https://developer.android.com/reference/android/support/v7/util/DiffUtil.html
 */
public class TransactionDiffCallback extends DiffUtil.Callback {

    private List<Transaction> oldTransactions;
    private List<Transaction> newTransactions;

    public TransactionDiffCallback(List<Transaction> oldTransactions, List<Transaction> newTransactions) {
        this.oldTransactions = oldTransactions;
        this.newTransactions = newTransactions;
    }

    @Override
    public int getOldListSize() {
        return oldTransactions.size();
    }

    @Override
    public int getNewListSize() {
        return newTransactions.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldTransactions.get(oldItemPosition).equals(newTransactions.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Transaction oldT = oldTransactions.get(oldItemPosition);
        Transaction newT = newTransactions.get(newItemPosition);

        return oldT.amount().equals(newT.amount()) &&
                oldT.date().equals(newT.date()) &&
                oldT.category().icon().equals(newT.category().icon()) &&
                oldT.category().name().equals(newT.category().name());
    }
}
