package com.dunk.mvvmspendalyzer.ui.common.transaction;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dunk.mvvmspendalyzer.domain.Transaction;
import com.dunk.mvvmspendalyzer.databinding.TransactionListItemBinding;

public class TransactionViewHolder extends RecyclerView.ViewHolder
    implements View.OnClickListener {

    private final TransactionListItemBinding mBinding;

    private Transaction mTransaction;

    private OnTransactionClickedListener mListener;

    public TransactionViewHolder(TransactionListItemBinding binding, OnTransactionClickedListener listener) {
        super(binding.getRoot());
        mBinding = binding;

        itemView.setOnClickListener(this);
        mListener = listener;
    }

    public void bind(Transaction t) {
        mTransaction = t;
        mBinding.setTransaction(t);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.onTransactionClicked(mTransaction.id());
        }
    }
}
