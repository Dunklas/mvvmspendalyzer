package com.dunk.mvvmspendalyzer.ui.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.databinding.AccountingBookBinding;
import com.dunk.mvvmspendalyzer.domain.AccountingPeriod;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;
import com.dunk.mvvmspendalyzer.util.SnackbarUtils;
import com.dunk.mvvmspendalyzer.viewmodels.AccountingPeriodViewModel;
import com.dunk.mvvmspendalyzer.viewmodels.DashboardViewModel;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * This fragment displays either a line chart (amount by date)
 * or a bar chart (amount by category), together with total expense,
 * income and balance.
 *
 * It only displays data within a specific date range, that is decided
 * by the AccountingPeriod provided when instantiating this fragment.
 */
public class AccountingPeriodFragment extends Fragment {

    private static String ARGUMENT_ACCOUNTING_BOOK = "ARG_ACCOUNTING_BOOK";

    // Max length of a category name (longer names will be clipped)
    private static final int MAX_CATEGORY_LENGTH = 7;

    // Max number of categories to be displayed in the bar chart
    private static final int MAX_CATEGORIES_DISPLAYED = 7;

    // ViewModel uniquely used by this fragment
    private AccountingPeriodViewModel mAccountingPeriodViewModel;

    // ViewModel shared with DashboardFragment
    private DashboardViewModel mDashboardViewModel;

    private AccountingBookBinding mViewDataBinding;

    private ViewGroup mLayout;
    private HorizontalBarChart mBarChart;
    private LineChart mLineChart;

    private CardView mTotalsCard;
    private CardView mChartCard;
    private ViewGroup mByDateContainer;
    private ViewGroup mByCategoryContainer;

    private TextView mNoExpensesLabel;

    /**
     * Returns an instance of AccountingPeriodFragment.
     *
     * @param   accountingPeriod  the AccountingPeriod (date range) to be displayed in this fragment
     */
    public static AccountingPeriodFragment newInstance(@NonNull AccountingPeriod accountingPeriod) {
        Bundle args = new Bundle();
        args.putSerializable(ARGUMENT_ACCOUNTING_BOOK, accountingPeriod);
        AccountingPeriodFragment fragment = new AccountingPeriodFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDashboardViewModel = DashboardFragment.obtainViewModel(getActivity());
        mAccountingPeriodViewModel = AccountingPeriodFragment.obtainViewModel(this);
        mAccountingPeriodViewModel.start( (AccountingPeriod) getArguments().getSerializable(ARGUMENT_ACCOUNTING_BOOK) );

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.accounting_period_frag, container, false);
        if (mViewDataBinding == null) {
            mViewDataBinding = AccountingBookBinding.bind(root);
        }
        mViewDataBinding.setViewmodel(mAccountingPeriodViewModel);

        return mViewDataBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLayout = view.findViewById(R.id.accounting_book_layout);
        mLineChart = view.findViewById(R.id.accounting_book_line_chart);
        mBarChart = view.findViewById(R.id.accounting_book_bar_chart);
        mByDateContainer = view.findViewById(R.id.accounting_book_by_date_container);
        mByCategoryContainer = view.findViewById(R.id.accounting_book_by_category_container);
        mTotalsCard = view.findViewById(R.id.accounting_book_totals_card);
        mChartCard = view.findViewById(R.id.accounting_book_chart_card);
        mNoExpensesLabel = view.findViewById(R.id.accounting_book_no_expenses_msg);
        setupObservers();
        updateChartVisibility(mDashboardViewModel.showByCategory.getValue());
    }

    // Called to toggle visibility of the two charts
    private void updateChartVisibility(boolean showByCategory) {
        if (showByCategory) {
            mByDateContainer.setVisibility(View.GONE);
            mByCategoryContainer.setVisibility(View.VISIBLE);
        } else {
            mByCategoryContainer.setVisibility(View.GONE);
            mByDateContainer.setVisibility(View.VISIBLE);
        }
    }

    private void setupObservers() {
        // This observable will be changed when we should toggle which chart that is displayed
        mDashboardViewModel.showByCategory.observe(this, b -> updateChartVisibility(b));

        // This observable will be changed when total by date figures has been updated
        mAccountingPeriodViewModel.totalByDate.observe(this, map -> updateLineGraph(map));

        // This observable will be changed when total by category figures has been updated
        mAccountingPeriodViewModel.totalByCategory.observe(this, map -> updateBarGraph(map));

        // This observable will be changed when an error should be displayed
        mAccountingPeriodViewModel.showErrorEvent.observe(this,
                (SnackbarMessage.SnackbarMessageObserver) error -> showError(error));

        // This observable will be changed when we detected that there are no expenses for this time period
        mAccountingPeriodViewModel.noExpensesEvent.observe(this, v -> showNoExpensesMsg());
    }

    // Prepare data and setup the bar chart
    private void updateBarGraph(Map<Category, BigDecimal> data) {
        if (data.size() <= 0) return;

        String[] labels = new String[data.size()];
        List<BarEntry> entries = new ArrayList<>();

        SortedMap<Category, BigDecimal> reversedMap = new TreeMap<>(Comparator.reverseOrder());
        reversedMap.putAll(data);
        int xCount = 0;
        for (Category key : reversedMap.keySet()) {
            entries.add(new BarEntry(xCount, reversedMap.get(key).floatValue()));
            labels[xCount++] = clipLabelIfNeccessary(key.name());
        }

        BarDataSet dataSet = new BarDataSet(entries, getString(R.string.dashboard_by_category_label));

        dataSet.setDrawValues(false);
        dataSet.setColor(getResources().getColor(R.color.colorPrimary, null));
        dataSet.setValueTextSize(14f);
        dataSet.setValueTypeface(Typeface.DEFAULT_BOLD);

        BarData barData = new BarData(dataSet);

        IAxisValueFormatter formatter = (value, axis) -> labels[(int)value];
        XAxis xAxis = mBarChart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);

        mBarChart.setTouchEnabled(true);
        mBarChart.setHighlightPerTapEnabled(true);
        mBarChart.setHighlightPerDragEnabled(false);
        mBarChart.setScaleXEnabled(false);
        mBarChart.setScaleYEnabled(false);

        mBarChart.setVerticalScrollBarEnabled(true);

        SMarkerView marker = new SMarkerView(getContext(), R.layout.marker_view);
        mBarChart.setMarker(marker);

        mBarChart.animateXY(1000, 1000);
        mBarChart.getDescription().setEnabled(false);
        mBarChart.setData(barData);

        mBarChart.moveViewTo(0, 0, YAxis.AxisDependency.LEFT);

        mBarChart.invalidate();
        mBarChart.setVisibleXRangeMaximum(MAX_CATEGORIES_DISPLAYED);
    }

    // Prepare data and setup the line chart
    private void updateLineGraph(Map<Calendar, BigDecimal> data) {
        if (data.size() <= 0) return;

        String[] labels = new String[data.size()];
        List<Entry> entries = new ArrayList<>();

        int xCount = 0;
        for (Calendar key : data.keySet()) {
            entries.add(new Entry(xCount, data.get(key).floatValue()));
            labels[xCount++] = String.valueOf(key.get(Calendar.DAY_OF_MONTH));
        }

        LineDataSet dataSet = new LineDataSet(entries, getString(R.string.dashboard_by_date_label));

        dataSet.setLineWidth(2.5f);
        dataSet.setDrawValues(false);
        dataSet.setColor(getResources().getColor(R.color.colorPrimary, null));
        dataSet.setCircleColor(getResources().getColor(R.color.colorPrimary, null));
        dataSet.setValueTextSize(7f);
        dataSet.setValueTypeface(Typeface.DEFAULT_BOLD);

        LineData lineData = new LineData(dataSet);

        IAxisValueFormatter formatter = (value, axis) -> labels[(int)value];
        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);
        xAxis.setTextSize(10f);

        mLineChart.setTouchEnabled(true);
        mLineChart.setHighlightPerTapEnabled(true);
        mLineChart.setHighlightPerDragEnabled(false);

        SMarkerView marker = new SMarkerView(getContext(), R.layout.marker_view);
        mLineChart.setMarker(marker);

        mLineChart.animateXY(1000, 1000);
        mLineChart.getDescription().setEnabled(false);
        mLineChart.setScaleXEnabled(false);
        mLineChart.setScaleYEnabled(false);
        mLineChart.setData(lineData);

        mLineChart.invalidate();
    }

    private void showError(@StringRes int errorMsg) {
        SnackbarUtils.message(mLayout, errorMsg);
    }

    private void showNoExpensesMsg() {
        mTotalsCard.setVisibility(View.GONE);
        mChartCard.setVisibility(View.GONE);
        mNoExpensesLabel.setText(R.string.dashboard_no_expenses);
        mNoExpensesLabel.setVisibility(View.VISIBLE);
    }

    /*
     * Note that by passing a fragment to ViewModelProviders, the ViewModel won't persist
     * over configuration changes, since the fragment is destroyed.
     */
    private static AccountingPeriodViewModel obtainViewModel(Fragment fragment) {
        ViewModelFactory factory = ViewModelFactory.getInstance(fragment.getActivity().getApplication());
        return ViewModelProviders.of(fragment, factory).get(AccountingPeriodViewModel.class);
    }

    private String clipLabelIfNeccessary(String label) {
        return label.length() > MAX_CATEGORY_LENGTH ? label.substring(0, MAX_CATEGORY_LENGTH) + "..." : label;
    }
}
