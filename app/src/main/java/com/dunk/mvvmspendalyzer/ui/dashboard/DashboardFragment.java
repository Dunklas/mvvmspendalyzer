package com.dunk.mvvmspendalyzer.ui.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.ui.addedittransaction.AddEditTransactionActivity;
import com.dunk.mvvmspendalyzer.util.TintUtils;
import com.dunk.mvvmspendalyzer.viewmodels.DashboardViewModel;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;

/**
 * Main fragment for the Dashboard.
 *
 * Displays a ViewPager with a number of AccountingPeriodFragment objects.
 * Includes a FloatingActionButton, as a 'shortcut' to create a new Transaction.
 *
 * Each AccountingPeriodFragment may display either a line chart (amount by date),
 * or a bar chart (amount by category).
 *
 * Which chart that is displayed is controlled by an option available in the AppBar of this fragment.
 * If this option changes, DashboardViewModel will be informed
 * Each AccountingPeriodFragment will observe the DashboardViewModel, in order to update themselves when
 * this value changes.
 *
 * I realize that this way of keeping track of which chart that should be displayed is quite messy,
 * but I haven't found any better solution to it. The hard part basically is to make sure that all
 * instantiated fragments in the ViewPager will be updated when the option changes (usually 3 fragments -
 * the active one, and the one to the left and right).
 */
public class DashboardFragment extends Fragment
    implements DashboardNavigator {

    private DashboardViewModel mViewModel;

    private ViewPager mViewPager;
    private DashboardPagerAdapter mPagerAdapter;

    private FloatingActionButton mFab;

    public static DashboardFragment newInstance() {
        return new DashboardFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = obtainViewModel(getActivity());
        mViewModel.start();

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dashboard_frag, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewPager = view.findViewById(R.id.dashboard_viewpager);
        mFab = view.findViewById(R.id.dashboard_fab);

        /* The ViewModel calculates date ranges to be used for this ViewPager.
         * This is a quite expensive calculation, and we therefore this
         * observable will be changed once it's completed.
         * TODO: Loading indicator?? */
        mViewModel.initialisationCompleted.observe(this, v -> setupViewPager());

        // This observable is changed when user want to add a new transaction
        mViewModel.clickNewTransactionEvent.observe(this, v -> onNewTransaction());

        setupFab();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.dashboard, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_dashboard_by_category:
                showByCategory(true);
                return true;
            case R.id.nav_dashboard_by_date:
                showByCategory(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Make sure correct option is visible, depending on which chart that is currently displayed
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.nav_dashboard_by_category).setVisible(!mViewModel.showByCategory.getValue());
        menu.findItem(R.id.nav_dashboard_by_date).setVisible(mViewModel.showByCategory.getValue());
    }

    // Notify mViewModel about which chart that should be displayed
    private void showByCategory(boolean showByCategory) {
        mViewModel.showByCategory.setValue(showByCategory);
        getActivity().invalidateOptionsMenu();
    }

    private void setupViewPager() {
        mPagerAdapter = new DashboardPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        // Change title each time the selected page changes
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int pos) {
                setTitle(pos);
            }
        });
        // Set current item to the one which includes 'today'
        mViewPager.setCurrentItem(mViewModel.indexOfToday());
    }

    private void setupFab() {
        Drawable icon = getResources().getDrawable(R.drawable.ic_add, null);
        TintUtils.tintDrawable(getContext(), icon, R.color.colorIconLight);
        mFab.setImageDrawable(icon);
        mFab.setOnClickListener(v -> mViewModel.newTransactionClicked());
    }

    private void setTitle(int pos) {

        // Provide a DateFormat to mViewModel, in order to get a localized title
        java.text.DateFormat df = DateFormat.getDateFormat(getActivity());
        String title = mViewModel.title(pos, df);

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    public static DashboardViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(DashboardViewModel.class);
    }

    @Override
    public void onNewTransaction() {
        Intent intent = new Intent(getActivity(), AddEditTransactionActivity.class);
        startActivity(intent);
    }

    /*
     * Since this is a FragmentStatePagerAdapter, it will only keep
     * maximum three fragments instantiated (active one, one to the left, one to the right).
     *
     * This adapter keeps no internal list of fragments, instead it relies on DashboardViewModel
     * to provide number of fragments, and AccountingPeriod (date range) for any new fragments
     * to be instantiated. */
    private class DashboardPagerAdapter extends FragmentStatePagerAdapter {

        public DashboardPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            Fragment fragment =  AccountingPeriodFragment.newInstance(mViewModel.period(pos));
            return fragment;
        }

        @Override
        public int getCount() {
            return mViewModel.numberPeriods();
        }

    }
}
