package com.dunk.mvvmspendalyzer.ui.dashboard;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface DashboardNavigator {

    /**
     * Called when user want to create a new Transaction.
     */
    void onNewTransaction();

}
