package com.dunk.mvvmspendalyzer.ui.dashboard;

import android.content.Context;
import android.widget.TextView;

import com.dunk.mvvmspendalyzer.R;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;

/**
 * A MarkerView used to display a label when user click
 * a bar/node in a chart.
 *
 * FYI! MarkerView extends RelativeLayout
 *
 * See: https://github.com/PhilJay/MPAndroidChart/wiki/IMarker-Interface
 */
public class SMarkerView extends MarkerView {

    private TextView mLabel;

    public SMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);
        mLabel = findViewById(R.id.marker_view_label);
    }

    // Invoked each time MarkerView is re-drawn
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        mLabel.setText(Utils.formatNumber(e.getY(), 0, false));
        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        // Centered horizontally
        float x = -(getWidth() / 2);

        // Above selected value
        float y = -getHeight();

        return new MPPointF(x, y);
    }
}
