package com.dunk.mvvmspendalyzer.ui.main;

import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.ui.about.AboutFragment;
import com.dunk.mvvmspendalyzer.ui.categories.CategoriesFragment;
import com.dunk.mvvmspendalyzer.ui.dashboard.DashboardFragment;
import com.dunk.mvvmspendalyzer.ui.settings.SettingsFragment;
import com.dunk.mvvmspendalyzer.ui.transactions.TransactionsFragment;
import com.dunk.mvvmspendalyzer.util.ActivityUtils;
import com.dunk.mvvmspendalyzer.util.TintUtils;

/**
 * Main activity of this app.
 *
 * Displays a Navigation Drawer and a number
 * of fragments.
 */
public class MainActivity extends AppCompatActivity implements MainNavigator {

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_act);

        setupToolbar();
        setupNavigationDrawer();
        setupDrawerToggle(mDrawerLayout, mToolbar);

        // If first time launching, set first item in menu
        if (savedInstanceState == null) {
            selectDrawerItem(mNavigationView.getMenu().findItem(R.id.nav_main_dashboard));
        }
    }

    private void setupToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void setupNavigationDrawer() {
        mDrawerLayout = findViewById(R.id.main_drawer_layout);
        mNavigationView= findViewById(R.id.main_nav_view);
        if (mNavigationView != null) {
            setupDrawerContent(mNavigationView);
        }
    }

    private void setupDrawerToggle(DrawerLayout drawerLayout, Toolbar toolbar) {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            // We need to consume this event to let ActionBarDrawerToggle handle it
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Change color of icons in AppBar
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        TintUtils.tintMenu(this, menu, R.color.colorIconDark);
        return true;
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener((item) -> {
            selectDrawerItem(item);
            return true;
        });
    }

    private void selectDrawerItem(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_main_dashboard:
                goToDashboard();
                break;
            case R.id.nav_main_transactions:
                goToTransactions();
                break;
            case R.id.nav_main_categories:
                goToCategories();
                break;
            case R.id.nav_main_settings:
                goToSettings();
                break;
            case R.id.nav_main_about:
                goToAbout();
                break;
            default:
                break;
        }

        // Close when item selected
        item.setChecked(true);
        mDrawerLayout.closeDrawers();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred (required for ActionBarDrawerToggle)
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles (required for ActionBarDrawerToggle)
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void goToDashboard() { goTo(DashboardFragment.newInstance(), DashboardFragment.class.getSimpleName()); }

    @Override
    public void goToTransactions() { goTo(TransactionsFragment.newInstance(), TransactionsFragment.class.getSimpleName()); }

    @Override
    public void goToCategories() { goTo(CategoriesFragment.newInstance(), CategoriesFragment.class.getSimpleName()); }

    @Override
    public void goToSettings() { goTo(SettingsFragment.newInstance(), SettingsFragment.class.getSimpleName()); }

    @Override
    public void goToAbout() { goTo(AboutFragment.newInstance(), AboutFragment.class.getSimpleName()); }

    /*
     * The reason why tag is required, is because I need it to get a reference
     * to DashboardFragment (see onBackPressed())
     */
    private void goTo(Fragment fragment, String tag) {
        ActivityUtils.replaceFragmentInActivity(
                getSupportFragmentManager(),
                fragment,
                R.id.main_content_frame,
                tag
        );
    }

    /*
     * Override back behavior. If navigation drawer is open, close it.
     * If any other view than Dashboard is open, navigate back to dashboard.
     */
    @Override
    public void onBackPressed() {
        Fragment dashboardFragment = getSupportFragmentManager().findFragmentByTag(DashboardFragment.class.getSimpleName());

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (dashboardFragment == null || !dashboardFragment.isVisible()) {
            selectDrawerItem(mNavigationView.getMenu().findItem(R.id.nav_main_dashboard));
        } else {
            super.onBackPressed();
        }
    }
}
