package com.dunk.mvvmspendalyzer.ui.main;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface MainNavigator {

    /**
     * Called when user select Dashboard in the main menu.
     */
    void goToDashboard();

    /**
     * Called when user select Transactions in the main menu.
     */
    void goToTransactions();

    /**
     * Called when user select Categories in the main menu.
     */
    void goToCategories();

    /**
     * Called when user select Settings in the main menu.
     */
    void goToSettings();

    /**
     * Called when user select About in the main menu.
     */
    void goToAbout();

}
