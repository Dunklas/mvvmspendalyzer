package com.dunk.mvvmspendalyzer.ui.pickcategory;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.data.model.CategoryDb;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;
import com.dunk.mvvmspendalyzer.ui.common.base.BaseActivity;
import com.dunk.mvvmspendalyzer.viewmodels.PickCategoryViewModel;

/**
 * Activity where a user may select a Category from a list.
 */
public class PickCategoryActivity extends BaseActivity
    implements PickCategoryNavigator {

    // Key used to add/obtain the picked Category from an Intent
    public static final String EXTRA_CATEGORY = "com.dunk.mvvmspendalyzer.ui.pickcategory.category";

    private PickCategoryViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = PickCategoryActivity.obtainViewModel(this);
        mViewModel.categorySelectedEvent.observe(this, c -> onCategoryPicked(c));
    }

    static PickCategoryViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(PickCategoryViewModel.class);
    }

    @Override
    public void onCategoryPicked(Category category) {
        Intent data = new Intent();
        data.putExtra(EXTRA_CATEGORY, category);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    protected Fragment fragment() {
        return PickCategoryFragment.newInstance();
    }

    @Override
    protected int title() {
        return R.string.category_pick_title;
    }
}
