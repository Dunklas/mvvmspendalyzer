package com.dunk.mvvmspendalyzer.ui.pickcategory;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.databinding.CategoryPickBinding;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;
import com.dunk.mvvmspendalyzer.ui.common.category.CategoryAdapter;
import com.dunk.mvvmspendalyzer.util.SnackbarUtils;
import com.dunk.mvvmspendalyzer.viewmodels.PickCategoryViewModel;

/**
 * Fragment where a user may select a Category from a list.
 */
public class PickCategoryFragment extends Fragment {

    private PickCategoryViewModel mViewModel;

    private RecyclerView mRecyclerView;
    private CategoryAdapter mAdapter;

    private CategoryPickBinding mViewDataBinding;

    public static PickCategoryFragment newInstance() {
        return new PickCategoryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = PickCategoryActivity.obtainViewModel(getActivity());
        mViewModel.start();

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.category_pick_frag, container, false);
        if (mViewDataBinding == null) {
            mViewDataBinding = CategoryPickBinding.bind(root);
        }
        mViewDataBinding.setViewmodel(mViewModel);

        return mViewDataBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setupRecyclerView(view);
        setupObservers();
    }

    private void setupRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.category_pick_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new CategoryAdapter(mRecyclerView);
        mAdapter.setOnCategoryClickedListener((c) -> mViewModel.categoryClicked(c));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupObservers() {
        // This observable is changed when an error message should be displayed
        mViewModel.showError.observe(this,
                (SnackbarMessage.SnackbarMessageObserver) (msg) -> showError(msg));
    }

    private void showError(@StringRes int errorMsg) {
        SnackbarUtils.message(mRecyclerView, errorMsg);
    }
}
