package com.dunk.mvvmspendalyzer.ui.pickcategory;

import com.dunk.mvvmspendalyzer.domain.Category;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface PickCategoryNavigator {

    /**
     * Called when a Category has been picked.
     *
     * @param category  the picked Category
     */
    void onCategoryPicked(Category category);

}
