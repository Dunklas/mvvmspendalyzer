package com.dunk.mvvmspendalyzer.ui.pickdate;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * DialogFragment where a user may select a date.
 */
public class PickDateFragment extends DialogFragment
    implements DatePickerDialog.OnDateSetListener {

    private static final String ARGUMENT_DATE = "DATE";

    /**
     * Interface to be implemented by the Activity/Fragment that
     * shows this DialogFragment.
     */
    public interface OnDatePickedListener {
        void onDatePicked(Calendar date);
    }

    /**
     * Returns an instance of PickDateFragment.
     *
     * @param date  the initial date to be displayed in the DatePicker
     */
    public static PickDateFragment newInstance(Calendar date) {
        Bundle b = new Bundle();
        b.putSerializable(ARGUMENT_DATE, date);
        PickDateFragment fragment = new PickDateFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!(getActivity() instanceof OnDatePickedListener)) {
            throw new RuntimeException("Parent activity must implement OnDatePickedListener.");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Calendar date = (Calendar) getArguments().getSerializable(ARGUMENT_DATE);
        date = date == null ? Calendar.getInstance() : date;

        int year = date.get(Calendar.YEAR);
        int month = date.get(Calendar.MONTH);
        int day = date.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, year);
        date.set(Calendar.MONTH, month);
        date.set(Calendar.DAY_OF_MONTH, day);

        ((OnDatePickedListener)getActivity()).onDatePicked(date);
        this.dismiss();
    }
}
