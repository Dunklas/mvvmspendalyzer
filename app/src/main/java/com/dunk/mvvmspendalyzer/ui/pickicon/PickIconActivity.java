package com.dunk.mvvmspendalyzer.ui.pickicon;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;
import com.dunk.mvvmspendalyzer.ui.common.base.BaseActivity;
import com.dunk.mvvmspendalyzer.viewmodels.PickIconViewModel;

/**
 * Activity where a user may select an icon from a list/grid.
 */
public class PickIconActivity extends BaseActivity
    implements PickIconNavigator {

    // Key used to add/obtain file name of the picked icon
    public static final String EXTRA_ICON_NAME = "com.dunk.mvvmspendalyzer.ui.pickicon.icon_name";

    private PickIconViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = PickIconActivity.obtainViewModel(this);
        mViewModel.iconSelectedEvent.observe(this, (i) -> onIconPicked(i));
    }

    static PickIconViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(PickIconViewModel.class);
    }

    @Override
    public void onIconPicked(String icon) {
        Intent data = new Intent();
        data.putExtra(EXTRA_ICON_NAME, icon);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    protected Fragment fragment() {
        return PickIconFragment.newInstance();
    }

    @Override
    protected @StringRes int title() {
        return R.string.icon_pick_title;
    }
}
