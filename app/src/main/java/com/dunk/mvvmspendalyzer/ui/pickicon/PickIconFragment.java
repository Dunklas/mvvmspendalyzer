package com.dunk.mvvmspendalyzer.ui.pickicon;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.databinding.IconPickBinding;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;
import com.dunk.mvvmspendalyzer.ui.common.icon.IconAdapter;
import com.dunk.mvvmspendalyzer.util.SnackbarUtils;
import com.dunk.mvvmspendalyzer.viewmodels.PickIconViewModel;

/**
 * Fragment where a user may select an icon from a list/grid.
 */
public class PickIconFragment extends Fragment {

    private PickIconViewModel mViewModel;

    private RecyclerView mRecyclerView;
    private IconAdapter mAdapter;

    private IconPickBinding mViewDataBinding;

    public static PickIconFragment newInstance() {
        return new PickIconFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = PickIconActivity.obtainViewModel(getActivity());
        mViewModel.start();

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.icon_pick_frag, container, false);
        if (mViewDataBinding == null) {
            mViewDataBinding = IconPickBinding.bind(root);
        }
        mViewDataBinding.setViewmodel(mViewModel);

        return mViewDataBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setupRecyclerView(view);
        setupObservers();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.icon_pick, menu);

        MenuItem searchItem = menu.findItem(R.id.nav_icon_pick_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        // Listen to changes of the search query, and notify mViewModel about any changes
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) { return false; }
            @Override
            public boolean onQueryTextChange(String newText) {
                mViewModel.searchQueryChanged(newText);
                return true;
            }
        });
    }

    private void setupRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.icon_pick_recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 5));

        mAdapter = new IconAdapter();
        mAdapter.setOnIconClickedListener((i) -> mViewModel.iconClicked(i));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupObservers() {
        // This observable will be changed if an error should be displayed
        mViewModel.showError.observe(this,
                (SnackbarMessage.SnackbarMessageObserver) (msg) -> showError(msg));
    }

    private void showError(@StringRes int errorMsg) {
        SnackbarUtils.message(mRecyclerView, errorMsg);
    }
}
