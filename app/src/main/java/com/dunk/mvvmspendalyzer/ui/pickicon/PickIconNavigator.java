package com.dunk.mvvmspendalyzer.ui.pickicon;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface PickIconNavigator {

    /**
     * Called when an icon has been picked.
     *
     * @param icon  file name of the picked icon
     */
    void onIconPicked(String icon);

}
