package com.dunk.mvvmspendalyzer.ui.picklocation;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.domain.Coordinate;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;
import com.dunk.mvvmspendalyzer.ui.common.base.BaseActivity;
import com.dunk.mvvmspendalyzer.util.LocationUtils;
import com.dunk.mvvmspendalyzer.viewmodels.PickLocationViewModel;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Activity where a user may select a location from a map.
 *
 * If this Activity is started with an Intent including the key
 * EXTRA_LOCATION, this location will be initially displayed.
 * If the value of EXTRA_LOCATION is null, users current location
 * will be obtained (if location permissions are or has previously been granted).
 *
 * This Activity uses PlaceAutocomplete in order to provide a search functionality.
 * See: https://developers.google.com/places/android-api/autocomplete
 */
public class PickLocationActivity extends BaseActivity
    implements PickLocationNavigator {

    public static final String EXTRA_LOCATION = "com.dunk.mvvmspendalyzer.ui.picklocation.location";

    private static final int REQUEST_PLACE_AUTOCOMPLETE = 0;

    private PickLocationViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = PickLocationActivity.obtainViewModel(this);

        mViewModel.searchPlace.observe(this, (v) -> onSearch());
        mViewModel.locationSelected.observe(this, (loc) -> onLocationPicked(loc));
    }

    @Override
    protected Fragment fragment() {
        Coordinate coordinate = (Coordinate) getIntent().getExtras().get(EXTRA_LOCATION);
        return PickLocationFragment.newInstance(coordinate);
    }

    @Override
    protected int title() {
        return R.string.location_pick_title;
    }

    static PickLocationViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(PickLocationViewModel.class);
    }

    @Override
    public void onLocationPicked(Coordinate coordinate) {
        Intent data = new Intent();
        data.putExtra(EXTRA_LOCATION, coordinate);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onSearch() {
        try {
            PlaceAutocomplete.IntentBuilder intentBuilder = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN);
            if (mViewModel.isLocationSet()) {
                LatLngBounds bias = LocationUtils.toBounds(
                        new LatLng(mViewModel.location.getValue().latitude(), mViewModel.location.getValue().longitude()), 2000);
                intentBuilder.setBoundsBias(bias);
            }
            Intent intent = intentBuilder.build(this);

            startActivityForResult(intent, REQUEST_PLACE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            mViewModel.onError(PickLocationError.GOOGLE_PLAY_SERVICES_NOT_AVAILABLE_REPAIRABLE, e);
        } catch (GooglePlayServicesNotAvailableException e) {
            mViewModel.onError(PickLocationError.GOOGLE_PLAY_SERVICES_NOT_AVAILABLE, e);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PLACE_AUTOCOMPLETE) {
            if (resultCode == Activity.RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(this, data);
                mViewModel.location.setValue(new Coordinate(place.getLatLng().latitude, place.getLatLng().longitude));

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                mViewModel.onError(PickLocationError.PLACE_AUTO_COMPLETE_RESULT_ERROR, null);
            }
        }
    }
}
