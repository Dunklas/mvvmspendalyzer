package com.dunk.mvvmspendalyzer.ui.picklocation;

/**
 * Possible errors that may occur while picking a location.
 */
public enum PickLocationError {

    GOOGLE_PLAY_SERVICES_NOT_AVAILABLE,
    GOOGLE_PLAY_SERVICES_NOT_AVAILABLE_REPAIRABLE,
    PLACE_AUTO_COMPLETE_RESULT_ERROR

}
