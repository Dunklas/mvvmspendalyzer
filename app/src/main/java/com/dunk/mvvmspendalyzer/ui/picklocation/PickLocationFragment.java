package com.dunk.mvvmspendalyzer.ui.picklocation;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.domain.Coordinate;
import com.dunk.mvvmspendalyzer.databinding.LocationPickBinding;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;
import com.dunk.mvvmspendalyzer.util.SnackbarUtils;
import com.dunk.mvvmspendalyzer.viewmodels.PickLocationViewModel;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Fragment where a user may select a location from a map.
 *
 * Previously, this fragment included a lot of logic related to the location/permission
 * process. However, I've changed so that PickLocationViewModel 'drives' this fragment
 * by invoking events. I think this makes the sequence of events more transparent,
 * even if it 'breaks' the MVVM pattern.
 */
public class PickLocationFragment extends Fragment {

    private PickLocationViewModel mViewModel;

    private static final String ARGUMENT_LOCATION = "LOCATION_COORDINATE";

    private static final String[] LOCATION_PERMISSIONS = new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    private static final int REQUEST_LOCATION_PERMISSIONS = 0;

    private LocationPickBinding mViewDataBinding;

    private FrameLayout mMapContainer;

    /**
     * Returns an instance of PickLocationFragment.
     *
     * @param coordinate    coordinate to initially be displayed on the map, may be null
     */
    public static PickLocationFragment newInstance(Coordinate coordinate) {
        Bundle args = new Bundle();
        args.putSerializable(ARGUMENT_LOCATION, coordinate);
        PickLocationFragment fragment = new PickLocationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = PickLocationActivity.obtainViewModel(getActivity());
        mViewModel.start((Coordinate)getArguments().get(ARGUMENT_LOCATION));

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.location_pick_frag, container, false);
        if (mViewDataBinding == null) {
            mViewDataBinding = LocationPickBinding.bind(root);
        }
        mViewDataBinding.setViewmodel(mViewModel);

        return mViewDataBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mMapContainer = view.findViewById(R.id.location_pick_map_container);
        setupMapFragment();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.location_pick, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.nav_location_pick_select).setEnabled(mViewModel.formValid.getValue());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_location_pick_search:
                mViewModel.doPlaceSearch();
                return true;
            case R.id.nav_location_pick_select:
                mViewModel.selectLocation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.location_pick_map_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.location_pick_map_container, mapFragment)
                    .commit();
        }
        mapFragment.getMapAsync((map) -> {
            setupObservers(map);
            mViewModel.onMapLoaded();
        });
    }

    private void setupObservers(GoogleMap googleMap) {
        // This observable will be changed if a location has been set (which makes the 'form' valid)
        mViewModel.formValid.observe(getActivity(), (b) -> getActivity().invalidateOptionsMenu());

        // Listen for long clicks and change the selected location
        googleMap.setOnMapLongClickListener((latLng -> mViewModel.location.setValue(new Coordinate(latLng.latitude, latLng.longitude))));

        // This observable will be changed if selected location has changed
        mViewModel.location.observe(PickLocationFragment.this, loc -> {
            if (loc != null) {
                LatLng latLng = new LatLng(loc.latitude(), loc.longitude());
                setMarker(googleMap, latLng);
                zoomToLocation(googleMap, latLng);
            }
        });

        // This observable will be changed when ViewModel tell us to verify permissions
        mViewModel.verifyPermissions.observe(PickLocationFragment.this, (v) -> {
            if (hasLocationPermissions()) {
                mViewModel.onHavePermissions();
            } else {
                mViewModel.onMissingPermissions();
            }
        });

        // This observable will be changed when ViewModel want us to check if a rationale message should be displayed
        mViewModel.shouldRationaleBeDisplayed.observe(PickLocationFragment.this, (v) -> {
           if (shouldDisplayPermissionRationale()) {
               mViewModel.onRationaleShouldBeDisplayed();
           } else {
               mViewModel.onRationaleShouldNotBeDisplayed();
           }
        });

        // This observable will be changed when ViewModel want us to display a rationale message
        mViewModel.showRationale.observe(PickLocationFragment.this, (msg, actionLabel, action) -> showRationale(msg, actionLabel, action));

        // This observable will be changed when ViewModel want us to request permissions
        mViewModel.requestPermissions.observe(PickLocationFragment.this, (v) -> requestPermissions());

        // This observable will be changed when ViewModel want us to obtain users current location
        mViewModel.obtainCurrentLocation.observe(PickLocationFragment.this, (v) -> obtainCurrentLocation());

        // This observable will be changed when an error message should be displayed
        mViewModel.showError.observe(PickLocationFragment.this, (SnackbarMessage.SnackbarMessageObserver) msg -> showError(msg));
    }

    private void setMarker(GoogleMap map, LatLng location) {
        if (map != null) {
            map.clear();
            map.addMarker(new MarkerOptions().position(location));
        }
    }

    private void zoomToLocation(GoogleMap map, LatLng location) {
        if (map != null) {
            CameraPosition position = new CameraPosition.Builder()
                    .target(location)
                    .zoom(15)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        }
    }

    private boolean hasLocationPermissions() {
        int result = ContextCompat
                .checkSelfPermission(getActivity(), LOCATION_PERMISSIONS[0]);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    /* This method performs no check if we have location permissions or not.
     * However, ViewModel makes sure we only call it once we KNOW that we have permissions */
    private void obtainCurrentLocation() {
        LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setNumUpdates(1);
        request.setInterval(0);
        LocationServices.getFusedLocationProviderClient(getActivity())
                .requestLocationUpdates(request, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        LatLng latLng = new LatLng(locationResult.getLastLocation().getLatitude(),
                                locationResult.getLastLocation().getLongitude());
                        mViewModel.location.setValue(
                                new Coordinate(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude()));
                    }
                }, null);
    }

    private boolean shouldDisplayPermissionRationale() {
        return ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), LOCATION_PERMISSIONS[0]) ||
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), LOCATION_PERMISSIONS[1]);
    }

    private void requestPermissions() {
        requestPermissions(LOCATION_PERMISSIONS,
                REQUEST_LOCATION_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSIONS:
                if (hasLocationPermissions()) {
                    mViewModel.onPermissionsGranted();
                } else {
                    mViewModel.onPermissionsDenied();
                }
        }
    }

    private void showRationale(@StringRes int message, @StringRes int actionLabel, Runnable action) {
        SnackbarUtils.action(mMapContainer, message, actionLabel, action);
    }

    private void showError(@StringRes int message) {
        SnackbarUtils.message(mMapContainer, message);
    }
}
