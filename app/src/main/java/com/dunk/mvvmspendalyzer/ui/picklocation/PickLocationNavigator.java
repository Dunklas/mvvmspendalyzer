package com.dunk.mvvmspendalyzer.ui.picklocation;

import com.dunk.mvvmspendalyzer.domain.Coordinate;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface PickLocationNavigator {

    /**
     * Called when a location has been picked.
     *
     * @param coordinate    coordinate of the picked location
     */
    void onLocationPicked(Coordinate coordinate);

    /**
     * Called when user want to search for a place
     * to be displayed on the map.
     */
    void onSearch();

}
