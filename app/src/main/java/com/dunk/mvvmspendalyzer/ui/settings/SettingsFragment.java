package com.dunk.mvvmspendalyzer.ui.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.resources.SettingsProvider;
import com.dunk.mvvmspendalyzer.util.DateUtils;

/**
 * Fragment to display a list of settings/preferences.
 * Preferences are defined in preferences.xml.
 */
public class SettingsFragment extends PreferenceFragmentCompat
    implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }

    @Override
    public void onResume() {
        super.onResume();

        setupToolbar();

        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);

        setSummaryDashboardPeriod();
        setSummaryFirstOfMonth();
    }

    @Override
    public void onPause() {
        super.onPause();

        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        // When any preference has been changed, update the summary of it to display the new value
        if (key.equals(getString(R.string.settings_dashboard_period_key))) {
            setSummaryDashboardPeriod();
        } else if (key.equals(R.string.settings_first_of_month_key)) {
            setSummaryFirstOfMonth();
        }
    }

    private void setSummaryDashboardPeriod() {
        Preference p = findPreference(getString(R.string.settings_dashboard_period_key));
        if (p instanceof ListPreference) {
            ListPreference lp = (ListPreference) p;

            String dashboardPeriod = lp.getEntry().toString();
            dashboardPeriod = dashboardPeriod.substring(0, 1).toLowerCase() + dashboardPeriod.substring(1);

            p.setSummary(getString(R.string.settings_dashboard_period_summary, dashboardPeriod));
        }
    }

    private void setSummaryFirstOfMonth() {
        Preference p = findPreference(getString(R.string.settings_first_of_month_key));
        if (p instanceof ListPreference) {
            ListPreference lp = (ListPreference) p;

            Integer firstDay = Integer.valueOf(lp.getEntry().toString());
            String suffix = DateUtils.dateSuffix(firstDay, getContext());

            p.setSummary(getString(R.string.settings_first_of_month_summary, firstDay, suffix));
        }
    }

    private void setupToolbar() {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.settings_title);
        }
    }
}
