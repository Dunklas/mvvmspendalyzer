package com.dunk.mvvmspendalyzer.ui.splash;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;
import com.dunk.mvvmspendalyzer.ui.main.MainActivity;
import com.dunk.mvvmspendalyzer.viewmodels.SplashViewModel;

/**
 * Activity to be used as a "splash screen".
 *
 * Currently, the only initialization that is done is to set
 * default settings/preferences. However, I'll keep it in case it'll be
 * needed in the future.
 */
public class SplashActivity extends AppCompatActivity implements SplashNavigator {

    private SplashViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_act);

        mViewModel = SplashActivity.obtainViewModel(this);

        mViewModel.startLoadingEvent.observe(this, (v) -> {
           // Display some kind of loading thingie??
        });

        // This observable is changed when ViewModel want us to set default settings.
        mViewModel.setDefaultSettings.observe(this, (v) ->
                PreferenceManager.setDefaultValues(this, R.xml.preferences, false));

        mViewModel.doneLoadingEvent.observe(this, (v) -> {
            // Stop display some kind of loading thingie??
            goToMain();
        });

        mViewModel.start();
    }

    public static SplashViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(SplashViewModel.class);
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
