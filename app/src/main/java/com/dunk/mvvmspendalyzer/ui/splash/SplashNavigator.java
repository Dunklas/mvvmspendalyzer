package com.dunk.mvvmspendalyzer.ui.splash;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface SplashNavigator {

    /**
     * Called when all initialization has been done and we should proceed
     * away from the Splash-view.
     */
    void goToMain();

}
