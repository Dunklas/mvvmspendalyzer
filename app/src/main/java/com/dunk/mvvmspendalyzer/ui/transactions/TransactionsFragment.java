package com.dunk.mvvmspendalyzer.ui.transactions;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.databinding.TransactionsBinding;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;
import com.dunk.mvvmspendalyzer.util.SnackbarUtils;
import com.dunk.mvvmspendalyzer.viewmodels.ViewModelFactory;
import com.dunk.mvvmspendalyzer.ui.addedittransaction.AddEditTransactionActivity;
import com.dunk.mvvmspendalyzer.ui.common.transaction.TransactionAdapter;
import com.dunk.mvvmspendalyzer.viewmodels.TransactionsViewModel;

/**
 * Fragment where a list of Transaction objects are displayed.
 */
public class TransactionsFragment extends Fragment
    implements TransactionsNavigator {

    private TransactionsViewModel mViewModel;

    private RecyclerView mRecyclerView;
    private TransactionAdapter mAdapter;

    private TransactionsBinding mBinding;

    public static TransactionsFragment newInstance() {
        return new TransactionsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewModel = TransactionsFragment.obtainViewModel(getActivity());
        mViewModel.start();

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.transactions_frag, container, false);
        if (mBinding == null) {
            mBinding = TransactionsBinding.bind(root);
        }
        mBinding.setViewmodel(mViewModel);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setupToolbar();
        setupRecyclerView(view);
        setupObservers();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.transactions, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_transactions_new:
                mViewModel.newTransactionClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupToolbar() {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.transactions_title);
        }
    }

    private void setupObservers() {
        // This observable will be changed when user click an existing transaction
        mViewModel.clickExistingTransactionEvent.observe(this, (tId) -> onExistingTransaction(tId));

        // This observable will be changed when user want to add a new transaction
        mViewModel.clickNewTransactionEvent.observe(this, (v) -> onNewTransaction());

        //This observable will be changed when an error should be displayed
        mViewModel.showError.observe(this,
                (SnackbarMessage.SnackbarMessageObserver) (msg) -> showError(msg));
    }

    private void setupRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.transactions_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new TransactionAdapter();
        mAdapter.setOnTransactionClickedListener((tId) -> mViewModel.existingTransactionClicked(tId));
        mRecyclerView.setAdapter(mAdapter);
    }

    public static TransactionsViewModel obtainViewModel(FragmentActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(TransactionsViewModel.class);
    }

    @Override
    public void onNewTransaction() {
        Intent intent = new Intent(getActivity(), AddEditTransactionActivity.class);
        startActivity(intent);
    }

    @Override
    public void onExistingTransaction(String transactionId) {
        Intent intent = new Intent(getActivity(), AddEditTransactionActivity.class);
        intent.putExtra(AddEditTransactionActivity.EXTRA_TRANSACTION_ID, transactionId);
        startActivity(intent);
    }

    private void showError(@StringRes int errorMsg) {
        SnackbarUtils.message(mRecyclerView, errorMsg);
    }
}
