package com.dunk.mvvmspendalyzer.ui.transactions;

/**
 * This interface is implemented by the Activity or Fragment
 * responsible for navigation.
 */
public interface TransactionsNavigator {

    /**
     * Called when user want to create a new Transaction.
     */
    void onNewTransaction();

    /**
     * Called when user want to open an existing Transaction.
     *
     * @param transactionId     id of the Transaction to be opened
     */
    void onExistingTransaction(String transactionId);

}
