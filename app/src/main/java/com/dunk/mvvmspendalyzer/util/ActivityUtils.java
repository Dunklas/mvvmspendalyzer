package com.dunk.mvvmspendalyzer.util;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Utility class for switching Fragment in a FragmentManager.
 */
public class ActivityUtils {

    public static void replaceFragmentInActivity(@NonNull FragmentManager fragmentManager,
                                                 @NonNull Fragment fragment, int frameId) {
        checkNotNull(fragmentManager);
        checkNotNull(fragment);

        fragmentManager
                .beginTransaction()
                .replace(frameId, fragment)
                .commit();

    }

    public static void replaceFragmentInActivity(@NonNull FragmentManager fragmentManager,
                                                 @NonNull Fragment fragment, int frameId, String tag) {
        checkNotNull(fragmentManager);
        checkNotNull(fragment);

        fragmentManager
                .beginTransaction()
                .replace(frameId, fragment, tag)
                .commit();
    }
}
