package com.dunk.mvvmspendalyzer.util;

import android.content.Context;

import com.dunk.mvvmspendalyzer.R;

import java.util.Calendar;

/**
 * Utility class for Date related methods.
 */
public class DateUtils {

    private DateUtils() {}

    /**
     * Returns a boolean indicating if c1 and c2 represents dates within the same month.
     */
    public static boolean isSameMonth(Calendar c1, Calendar c2) {
        return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
                c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH);
    }

    /**
     * Returns a boolean indicating if c1 and c2 represents dates within the same week.
     */
    public static boolean isSameWeek(Calendar c1, Calendar c2) {
        return c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
                c1.get(Calendar.WEEK_OF_YEAR) == c2.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * Returns a clone of date, where time properties are set to 00:00:00:00.
     */
    public static Calendar toMidnight(Calendar date) {
        date = (Calendar) date.clone();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        return date;
    }

    /**
     * Returns a Calendar object representing the first day of the of a monthly period,
     * "seeded" by rightNow.
     *
     * @param firstDay  first day of month to be used for the calculation
     * @param rightNow  current date
     */
    public static Calendar firstDateOfMonth(int firstDay, Calendar rightNow) {
        Calendar cal = (Calendar) rightNow.clone();

        int currentDate = cal.get(Calendar.DAY_OF_MONTH);

        if (currentDate >= firstDay) {
            cal.set(Calendar.DAY_OF_MONTH, firstDay);
        } else {
            cal.add(Calendar.MONTH, -1);
            cal.set(Calendar.DAY_OF_MONTH, firstDay);
        }

        return DateUtils.toMidnight(cal);
    }

    /**
     * Returns a Calendar object representing the first day of a bi weekly period,
     * "seeded" by rightNow.
     *
     * @param rightNow  current date
     */
    public static Calendar firstDateOfBiWeekly(Calendar rightNow) {
        Calendar cal = (Calendar) rightNow.clone();

        // For bi-weekly periods I consider odd weeks to be the first of the two weeks
        // If current week is odd...
        if (cal.get(Calendar.WEEK_OF_YEAR) % 2 != 0) {
            // Iterate backwards until you encounter first day of the week
            while(cal.get(Calendar.DAY_OF_WEEK) != cal.getFirstDayOfWeek()) {
                cal.add(Calendar.DAY_OF_WEEK, -1);
            }

            // And if current week is even...
        } else {
            // Subtract one week..
            cal.add(Calendar.WEEK_OF_YEAR, -1);
            // And iterate backwards until you encounter first day of the week
            while (cal.get(Calendar.DAY_OF_WEEK) != cal.getFirstDayOfWeek()) {
                cal.add(Calendar.DAY_OF_WEEK, -1);
            }
        }

        return toMidnight(cal);
    }

    /**
     * Returns a Calendar object representing the first day of a weekly period,
     * "seeded" by rightNow.
     *
     * @param rightNow  current date
     */
    public static Calendar firstDateOfWeek(Calendar rightNow) {
        Calendar cal = (Calendar) rightNow.clone();

        while (cal.get(Calendar.DAY_OF_WEEK) != cal.getFirstDayOfWeek()) {
            cal.add(Calendar.DAY_OF_WEEK, -1);
        }

        return DateUtils.toMidnight(cal);
    }

    /**
     * Returns a suffix to be used for a date string, such as "st" (1st), "nd" (2nd), "rd" (3rd), etc.
     */
    public static String dateSuffix(int date, Context ctx) {
        if (date >= 11 && date <= 13) {
            return ctx.getString(R.string.date_suffix_other);
        }

        switch (date % 10) {
            case 1: return ctx.getString(R.string.date_suffix_1);
            case 2: return ctx.getString(R.string.date_suffix_2);
            case 3: return ctx.getString(R.string.date_suffix_3);
            default: return ctx.getString(R.string.date_suffix_other);
        }
    }
}
