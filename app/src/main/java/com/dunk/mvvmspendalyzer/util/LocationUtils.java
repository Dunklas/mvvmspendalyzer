package com.dunk.mvvmspendalyzer.util;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.SphericalUtil;

/**
 * Utility class for location/coordinate related methods.
 */
public class LocationUtils {

    private LocationUtils() {}

    /**
     * Returns a LatLngBounds-object of given center and radiusInMeters.
     * This is used by PlaceAutocomplete (see PickLocationActivity) in order to bias a search for a
     * given location.
     */
    public static LatLngBounds toBounds(LatLng center, double radiusInMeters) {
        double distanceFromCenterToCorner = radiusInMeters * Math.sqrt(2.0);
        LatLng southwestCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 225.0);
        LatLng northeastCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 45.0);
        return new LatLngBounds(southwestCorner, northeastCorner);
    }
}
