package com.dunk.mvvmspendalyzer.util;

import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Utility class for Snackbars.
 */
public class SnackbarUtils {

    private SnackbarUtils() {}

    /**
     * Displays a String resource in a Snackbar.
     */
    public static void message(View view, @StringRes int messageResource) {
        Snackbar.make(view, messageResource,
                Snackbar.LENGTH_LONG)
                .show();
    }

    /**
     * Displays a String resource with an action in a Snackbar.
     * actionLabel is used as name for the action and action is executed
     * when the actionLabel is clicked.
     */
    public static void action(View view, @StringRes int messageResource, @StringRes int actionLabel, Runnable action) {
        Snackbar.make(view, messageResource,
                Snackbar.LENGTH_LONG)
                .setAction(actionLabel, (v) -> action.run())
                .show();
    }
}
