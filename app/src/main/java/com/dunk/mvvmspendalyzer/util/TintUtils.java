package com.dunk.mvvmspendalyzer.util;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Utility class to change tint of Drawables.
 */
public class TintUtils {

    private TintUtils() {}

    /**
     * Tints the icon of every item in menu.
     */
    public static void tintMenu(Context ctx, Menu menu, @ColorRes int color) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            tintMenuItem(ctx, item, color);
        }
    }

    private static void tintMenuItem(Context ctx, MenuItem item, @ColorRes int color) {
        Drawable icon = item.getIcon();
        if (icon != null) {
            tintDrawable(ctx, item.getIcon(), color);
        }
    }

    /**
     * Tints drawable to color.
     */
    public static void tintDrawable(Context ctx, Drawable drawable, @ColorRes int color) {
        DrawableCompat.setTint(drawable, ctx.getColor(color));
    }
}
