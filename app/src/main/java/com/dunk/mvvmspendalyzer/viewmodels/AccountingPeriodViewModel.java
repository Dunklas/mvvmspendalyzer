package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.StringRes;
import android.util.Log;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.data.TransactionRepository;
import com.dunk.mvvmspendalyzer.domain.AccountingPeriod;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.domain.CategoryType;
import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.exceptions.CompositeException;

/**
 * ViewModel used for a view where data
 * visualization is displayed for one
 * specific time period.
 *
 * The data loaded/displayed is:
 *  - Total amount (expenses), by category
 *  - Total amount (expenses), by date
 *  - Total amount (expenses)
 *  - Total amount (income)
 */
public class AccountingPeriodViewModel extends ViewModel {

    private TransactionRepository mTransactionRepository;

    // RxJava subscription are collected in mDisposable.
    // mDisposable is cleared in onCleared() to avoid memory leaks
    private CompositeDisposable mDisposable = new CompositeDisposable();

    // Total expenses, by date and by category, during this time period
    public final MutableLiveData<Map<Calendar, BigDecimal>> totalByDate = new MutableLiveData<>();
    public final MutableLiveData<Map<Category, BigDecimal>> totalByCategory = new MutableLiveData<>();

    // Total income, expense and balance, during this period
    public final ObservableField<BigDecimal> totalIncome = new ObservableField<>();
    public final ObservableField<BigDecimal> totalExpense = new ObservableField<>();
    public final ObservableField<BigDecimal> balance = new ObservableField<>();

    // Indicates if any data is currently loading
    public final ObservableBoolean totalsLoading = new ObservableBoolean(false);
    public final ObservableBoolean byDateLoading = new ObservableBoolean(false);
    public final ObservableBoolean byCategoryLoading = new ObservableBoolean(false);

    // Event fired when an error message should be displayed
    public final SnackbarMessage showErrorEvent = new SnackbarMessage();

    // Event fired if there are no expenses for this time period
    public final SingleLiveEvent<Void> noExpensesEvent = new SingleLiveEvent<>();

    private boolean mIsTotalsLoaded;
    private boolean mIsByDateLoaded;
    private boolean mIsByCategoryLoaded;

    // Holds date ranges for this time period
    private AccountingPeriod mAccountingPeriod;

    private Observable.OnPropertyChangedCallback mTotalChangeCallback;

    public AccountingPeriodViewModel(TransactionRepository transactionRepository) {
        mTransactionRepository = transactionRepository;

        // Observe totalExpense, to see if noExpensesEvent should be called
        mTotalChangeCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (totalExpense.get().compareTo(new BigDecimal(0)) <= 0) {
                    noExpensesEvent.call();
                }
            }
        };
        totalExpense.addOnPropertyChangedCallback(mTotalChangeCallback);
    }

    // Invoked by UI to start/init this ViewModel
    public void start(AccountingPeriod accountingPeriod) {

        mAccountingPeriod = accountingPeriod;

        // If we have not loaded or are not currently loading totals...Do it.
        if (!totalsLoading.get() && !mIsTotalsLoaded) {
            totalsLoading.set(true);
            loadTotalIncome();
            loadTotalExpense();
        }

        // If we have not loaded or are not currently loading total amount by date...Do it.
        if (!byDateLoading.get() && !mIsByDateLoaded) {
            byDateLoading.set(true);
            loadByDate();
        }

        // If we have not loaded or are not currently loading total amount by category...Do it.
        if (!byCategoryLoading.get() || !mIsByCategoryLoaded) {
            byCategoryLoading.set(true);
            loadByCategory();
        }
    }

    private void loadByDate() {
        mDisposable.add(mTransactionRepository.totalByDate(mAccountingPeriod.from(), mAccountingPeriod.to(), CategoryType.EXPENSE)
                .map(totalByDate -> {
                    // Obtain the full date range for this time period and initialize with amount 0 for each date
                    // This is required because date range obtained from repository only contain dates where transactions has occurred
                    Map<Calendar, BigDecimal> totalsTmp = new TreeMap<>();
                    for (Calendar c : mAccountingPeriod.range()) {
                        totalsTmp.put(c, new BigDecimal(0));
                    }
                    // Add data obtained from repository
                    totalsTmp.putAll(totalByDate);
                    return totalsTmp;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(map -> byDateLoaded(map), e -> handleException(R.string.dashboard_error_load, e)));
    }

    private void loadByCategory() {
        mDisposable.add(mTransactionRepository.totalByCategory(mAccountingPeriod.from(), mAccountingPeriod.to(), CategoryType.EXPENSE)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(map -> byCategoryLoaded(map), e -> handleException(R.string.dashboard_error_load, e)));
    }

    private void loadTotalIncome() {
        mDisposable.add(mTransactionRepository.total(mAccountingPeriod.from(), mAccountingPeriod.to(), CategoryType.INCOME)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(total -> totalIncomeLoaded(total), e -> handleException(R.string.dashboard_error_load, e)));
    }

    private void loadTotalExpense() {
        mDisposable.add(mTransactionRepository.total(mAccountingPeriod.from(), mAccountingPeriod.to(), CategoryType.EXPENSE)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(total -> totalExpenseLoaded(total), e -> handleException(R.string.dashboard_error_load, e)));
    }

    // Invoked when total income has been loaded
    private void totalIncomeLoaded(BigDecimal total) {
        this.totalIncome.set(total);
        if (totalExpense.get() != null) {
            // If expense is loaded too, calculate balance
            this.balance.set(totalIncome.get().subtract(totalExpense.get()));
            totalsLoading.set(false);
            mIsTotalsLoaded = true;
        }
    }

    // Invoked when total expenses has been loaded
    private void totalExpenseLoaded(BigDecimal total) {
        this.totalExpense.set(total);
        if (totalIncome.get() != null) {
            // If income is loaded too, calculate balance
            this.balance.set(totalIncome.get().subtract(totalExpense.get()));
            totalsLoading.set(false);
            mIsTotalsLoaded = false;
        }
    }

    // Invoked when amount by date has been loaded
    private void byDateLoaded(Map<Calendar, BigDecimal> totalByDate) {
        this.totalByDate.setValue(totalByDate);
        byDateLoading.set(false);
        mIsByDateLoaded = true;
    }

    // Invoked when amount by category has been loaded
    private void byCategoryLoaded(Map<Category, BigDecimal> totalByCategory) {
        this.totalByCategory.setValue(totalByCategory);
        byCategoryLoading.set(false);
        mIsByCategoryLoaded = true;
    }

    // TODO: Investigate how to handle exceptions in a good way. They're all put in a CompositeException object and I'm not sure what best practices are
    private void handleException(@StringRes int errorMsg, Throwable t) {
        showErrorEvent.setValue(errorMsg);
    }

    @Override
    public void onCleared() {
        mDisposable.clear();
        totalExpense.removeOnPropertyChangedCallback(mTotalChangeCallback);
    }
}
