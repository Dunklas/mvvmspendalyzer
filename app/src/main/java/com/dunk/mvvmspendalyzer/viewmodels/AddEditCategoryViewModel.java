package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.data.CategoryAlreadyExistException;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.data.CategoryRepository;
import com.dunk.mvvmspendalyzer.domain.CategoryType;
import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.exceptions.CompositeException;

/**
 * ViewModel used for a view where a
 * Category may be created or modified.
 */
public class AddEditCategoryViewModel extends ViewModel {

    private CategoryRepository mCategoryRepository;

    @Nullable // Will be null if this ViewModel is used for creation of a new Category
    private String mCategoryId;

    // Properties for the new/modified category
    public final ObservableField<String> name = new ObservableField<>();
    public final ObservableField<CategoryType> type = new ObservableField<>();
    public final ObservableField<String> icon = new ObservableField<>();

    // Indicates if data is currently loading
    public final ObservableBoolean dataLoading = new ObservableBoolean(false);

    // Indicates if this form is valid
    public final MutableLiveData<Boolean> formValid = new MutableLiveData<>();

    // Event fired if a category with this name already exist
    public final SnackbarMessage categoryAlreadyExistEvent = new SnackbarMessage();

    // Event fired if an error message should be displayed
    public final SnackbarMessage showErrorEvent = new SnackbarMessage();

    // Event fired when user want to save this category
    public final SingleLiveEvent<Void> saveCategoryEvent = new SingleLiveEvent<>();

    // Event fired when user want to select an icon for this category
    public final SingleLiveEvent<Void> selectIconEvent = new SingleLiveEvent<>();

    private boolean mIsDataLoaded;

    private Observable.OnPropertyChangedCallback mValidationCallback;

    public AddEditCategoryViewModel(CategoryRepository categoryRepository) {
        mCategoryRepository = categoryRepository;

        formValid.setValue(false);
        addValidationCallback();
    }

    // Run validation each time a property is changed
    private void addValidationCallback() {
        mValidationCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                validate();
            }
        };
        name.addOnPropertyChangedCallback(mValidationCallback);
        type.addOnPropertyChangedCallback(mValidationCallback);
        icon.addOnPropertyChangedCallback(mValidationCallback);
    }

    /* Invoked by UI to start/init this ViewModel
     * May be started with or without a categoryId */
    public void start(String categoryId) {
        if (dataLoading.get()) {
            return;
        }

        if (mIsDataLoaded) {
            validate();
            return;
        }

        mCategoryId = categoryId;
        if (mCategoryId == null) {
            mIsDataLoaded = true; // No data to load since this is a new category, therefore setting to true
            setDefaultValues();
            return;
        }

        dataLoading.set(true);
        loadCategory();
    }

    // Invoked to set default values of a new category
    private void setDefaultValues() {
        name.set(null); // Explicit null values to be consistent with AddEditTransactionViewModel
        type.set(CategoryType.EXPENSE);
        icon.set(null);
    }

    private void loadCategory() {
        mCategoryRepository.category(mCategoryId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((c) -> categoryLoaded(c), (error) -> handleException(R.string.category_addedit_error_load, error));
    }

    // Invoked when category has been loaded
    private void categoryLoaded(Category c) {
        name.set(c.name());
        type.set(c.type());
        icon.set(c.icon());

        dataLoading.set(false);
        mIsDataLoaded = true;
    }

    // Invoked by UI when user want to save this category
    public void saveCategory() {

        if (!formValid.getValue()) return;

        Category savedCategory = new Category(mCategoryId, name.get(), type.get(), icon.get());

        // TODO: Maybe data layer can decide if this should be an insert or update?
        if (mCategoryId == null) {
            mCategoryRepository.insert(savedCategory)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> saveCategoryEvent.call(), (error) -> handleException(R.string.category_addedit_error_save, error));
        } else {
            mCategoryRepository.update(savedCategory)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> saveCategoryEvent.call(), (error) -> handleException(R.string.category_addedit_error_save, error));
        }
    }

    // Invoked by UI to select an icon for this category
    public void setIcon(String name) {
        icon.set(name);
    }

    // Invoked by UI when user navigate to view for selecting an icon
    public void selectIcon() {
        selectIconEvent.call();
    }

    // Invoked to validate category properties
    private void validate() {
        if (Category.isValid(mCategoryId, name.get(), type.get(), icon.get())) {
            formValid.setValue(true);
        } else {
            formValid.setValue(false);
        }
    }

    private void handleException(@StringRes int errorMsg, Throwable t) {
        if (t instanceof CompositeException) {
            for (Throwable subT : ((CompositeException) t).getExceptions()) {
                if (subT instanceof CategoryAlreadyExistException) {
                    categoryAlreadyExistEvent.setValue(R.string.category_addedit_error_already_exist);
                    return;
                }
            }
        }
        showErrorEvent.setValue(errorMsg);
    }

    @Override
    protected void onCleared() {
        name.removeOnPropertyChangedCallback(mValidationCallback);
        type.removeOnPropertyChangedCallback(mValidationCallback);
        icon.removeOnPropertyChangedCallback(mValidationCallback);
    }
}
