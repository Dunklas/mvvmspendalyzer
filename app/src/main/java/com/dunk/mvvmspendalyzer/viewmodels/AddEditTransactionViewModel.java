package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.data.TransactionRepository;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.domain.Coordinate;
import com.dunk.mvvmspendalyzer.domain.Transaction;
import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;

import java.math.BigDecimal;
import java.util.Calendar;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * ViewModel used for a view where a
 * Transaction may be created or modified.
 */
public class AddEditTransactionViewModel extends ViewModel {

    private TransactionRepository mTransactionRepository;

    @Nullable // Will be null if this ViewModel is used for creation of a new Transaction
    private String mTransactionId;

    // Properties for the new/modified transaction
    public final ObservableField<BigDecimal> amount = new ObservableField<>();
    public final ObservableField<Category> category = new ObservableField<>();
    public final ObservableField<Calendar> date = new ObservableField<>();
    public final ObservableField<String> note = new ObservableField<>();
    public final MutableLiveData<Coordinate> location = new MutableLiveData<>();

    // Indicates if data is currently loading
    public final ObservableBoolean dataLoading = new ObservableBoolean(false);

    // Indicates if this form is valid
    public final MutableLiveData<Boolean> formValid = new MutableLiveData<Boolean>();

    // Event fired when an error should be displayed
    public final SnackbarMessage showErrorEvent = new SnackbarMessage();

    // Event fired when user want to save this transaction
    public final SingleLiveEvent<Void> saveTransactionEvent = new SingleLiveEvent<>();

    // Event fired when user want to select a category for this transaction
    public final SingleLiveEvent<Void> selectCategoryEvent = new SingleLiveEvent<>();

    // Event fired when user want to select a date for this transaction
    public final SingleLiveEvent<Calendar> selectDateEvent = new SingleLiveEvent<>();

    // Event fired when user want to select a location for this transaction
    public final SingleLiveEvent<Coordinate> selectLocationEvent = new SingleLiveEvent<>();

    private boolean mIsDataLoaded;

    private Observable.OnPropertyChangedCallback mValidationCallback;

    public AddEditTransactionViewModel(TransactionRepository transactionRepository) {
        mTransactionRepository = transactionRepository;
        formValid.setValue(false);
        addValidationCallback();
    }

    // Run validation each time a non-optional property is changed
    private void addValidationCallback() {
        mValidationCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                validate();
            }
        };
        amount.addOnPropertyChangedCallback(mValidationCallback);
        category.addOnPropertyChangedCallback(mValidationCallback);
        date.addOnPropertyChangedCallback(mValidationCallback);
    }

    /* Invoked by UI to start/init this ViewModel
     * May be started with or without a transactionId */
    public void start(String transactionId) {
        if (dataLoading.get()) {
            return;
        }

        if (mIsDataLoaded) {
            return;
        }

        mTransactionId = transactionId;
        if (mTransactionId == null) {
            mIsDataLoaded = true; // No data to load, since this is a new transaction
            setDefaultValues();
            return;
        }

        dataLoading.set(true);
        loadTransaction();
    }

    /* Invoked to set default values of a new transaction
     * The default values for these are null, but I set them to null explicit
     * because I want the observable fields to initially "fire off" for null values too.
     * A view may need to be displayed differently depending on what properties are set,
     * therefore it's valuable to make UI aware that the value is null. */
    private void setDefaultValues() {
        amount.set(null);
        category.set(null);
        date.set(Calendar.getInstance());
        note.set(null);
        location.setValue(null);
    }

    private void loadTransaction() {
        mTransactionRepository.transaction(mTransactionId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(t -> transactionLoaded(t), error -> handleException(R.string.transaction_addedit_error_load, error));
    }

    // Invoked when transaction has been loaded
    private void transactionLoaded(Transaction transaction) {
        amount.set(transaction.amount());
        category.set(transaction.category());
        date.set(transaction.date());
        note.set(transaction.note());
        location.setValue(transaction.coordinate());

        dataLoading.set(false);
        mIsDataLoaded = true;
    }

    // Invoked to validate transaction properties
    private void validate() {
        if (Transaction.isValid(mTransactionId, category.get(), amount.get(), date.get(), note.get(), location.getValue())) {
            formValid.setValue(true);
        } else {
            formValid.setValue(false);
        }
    }

    // Invoked by UI to set a category for this transaction
    public void setCategory(Category category) {
        this.category.set(category);
    }

    // Invoked by UI to set a date for this transaction
    public void setDate(Calendar date) {
        this.date.set(date);
    }

    // Invoked by UI to set a location for this transaction
    public void setLocation(Coordinate coordinate) { this.location.setValue(coordinate); }

    // Invoked by UI to remove the location set for this transaction
    public void removeLocation() {
        this.location.setValue(null);
    }

    // Invoked by UI when user want to select a category for this transaction
    public void selectCategory() {
        selectCategoryEvent.call();
    }

    // Invoked by UI when user want to select a date for this transaction
    public void selectDate() { selectDateEvent.setValue(date.get()); }

    // Invoked by UI when user want to select a location for this transaction
    public void selectLocation() { selectLocationEvent.setValue(location.getValue()); }

    // Invoked by UI to save this transaction
    public void saveTransaction() {

        if (!formValid.getValue()) return;

        Transaction savedTransaction = new Transaction(
                mTransactionId, category.get(), amount.get(), date.get(), note.get(), location.getValue());

        if (mTransactionId == null) {
            mTransactionRepository.insert(savedTransaction)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> saveTransactionEvent.call(), error -> handleException(R.string.transaction_addedit_error_save, error));
        } else {
            mTransactionRepository.update(savedTransaction)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> saveTransactionEvent.call(), error -> handleException(R.string.transaction_addedit_error_save, error));
        }
    }

    private void handleException(@StringRes int errorMsg, Throwable t) {
        showErrorEvent.setValue(errorMsg);
    }

    @Override
    protected void onCleared() {
        amount.removeOnPropertyChangedCallback(mValidationCallback);
        category.removeOnPropertyChangedCallback(mValidationCallback);
        date.removeOnPropertyChangedCallback(mValidationCallback);
    }
}
