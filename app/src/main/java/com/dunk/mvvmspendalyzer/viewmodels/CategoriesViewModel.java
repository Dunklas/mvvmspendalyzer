package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableList;
import android.support.annotation.StringRes;
import android.util.Log;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.data.CategoryRepository;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.logging.Tags;
import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ViewModel used for a view where a list of
 * Category objects are displayed.
 */
public class CategoriesViewModel extends ViewModel {

    private final CategoryRepository mCategoryRepository;

    // RxJava subscription are collected in mDisposable.
    // mDisposable is cleared in onCleared() to avoid memory leaks
    private CompositeDisposable mDisposable = new CompositeDisposable();

    public final ObservableList<Category> categories = new ObservableArrayList<>();
    public final ObservableBoolean dataLoading = new ObservableBoolean(false);

    // Event fired when user click a category in the list
    public final SingleLiveEvent<String> clickExistingCategoryEvent = new SingleLiveEvent<>();

    // Event fired when user click to create a new category
    public final SingleLiveEvent<Void> clickNewCategoryEvent = new SingleLiveEvent<>();

    // Event fired when an error message should be displayed
    public final SnackbarMessage showError = new SnackbarMessage();

    private boolean mIsDataLoaded;

    public CategoriesViewModel(CategoryRepository categoryRepository) {
        mCategoryRepository = categoryRepository;
    }

    // Invoked by UI to start/init this ViewModel
    public void start() {
        if (dataLoading.get()) {
            return;
        }

        if (mIsDataLoaded) {
            return;
        }

        dataLoading.set(true);
        loadCategories();
    }

    private void loadCategories() {
        mDisposable.add(mCategoryRepository.categories()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> categoriesLoaded(list), error -> handleException(R.string.categories_error_load, error)));
    }

    // Invoked when categories has been loaded
    private void categoriesLoaded(List<Category> categories) {
        this.categories.clear();
        this.categories.addAll(categories);
        dataLoading.set(false);
        mIsDataLoaded = true;
    }

    // Called by UI layer to fire this event
    public void existingCategoryClicked(String categoryId) {
        clickExistingCategoryEvent.setValue(categoryId);
    }

    // Called by UI layer to fire this event
    public void newCategoryClicked() {
        clickNewCategoryEvent.call();
    }

    private void handleException(@StringRes int errorMsg, Throwable t) {
        showError.setValue(errorMsg);
    }

    @Override
    protected void onCleared() {
        mDisposable.clear();
    }
}
