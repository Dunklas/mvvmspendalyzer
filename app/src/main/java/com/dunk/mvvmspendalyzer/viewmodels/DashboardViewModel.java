package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.Handler;

import com.dunk.mvvmspendalyzer.domain.AccountingPeriod;
import com.dunk.mvvmspendalyzer.domain.AccountingPeriodManager;
import com.dunk.mvvmspendalyzer.domain.AccountingPeriodType;
import com.dunk.mvvmspendalyzer.resources.SettingsProvider;
import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;

import java.text.DateFormat;
import java.util.Calendar;

/**
 * ViewModel used for a view where a dashboard
 * with data visualizations for several time
 * periods are displayed.
 */
public class DashboardViewModel extends ViewModel {

    private SettingsProvider mSettingsProvider;

    // Helper object to instantiate AccountingPeriod objects (date ranges)
    private AccountingPeriodManager mAccountingPeriodManager;

    /* Event fired when user want to see a "by category" visualization of the data.
     * Default is to display a "by date" visualization". This boolean indicates if "by category"
     * should be displayed or not.
     * TODO: Find a better way to toggle between these two views */
    public final MutableLiveData<Boolean> showByCategory = new MutableLiveData<>();

    // Event fired when user want to create a new transaction
    public final SingleLiveEvent<Void> clickNewTransactionEvent = new SingleLiveEvent<>();

    // Event fired when AccountingPeriod objects (date ranges) has been calculated
    public final SingleLiveEvent<Void> initialisationCompleted = new SingleLiveEvent<>();

    public DashboardViewModel(SettingsProvider settingsProvider) {
        mSettingsProvider = settingsProvider;
    }

    // Called by UI layer to start/init this ViewModel
    public void start() {
        if (showByCategory.getValue() == null) {
            showByCategory.setValue(false);
        }

        /* These calculations seem to take quite some time...
         * Therefore, doing it on a background thread.
         * TODO: Loading indicator... */
        final Handler handler = new Handler();
        new Thread(() -> {
            AccountingPeriodType period = mSettingsProvider.dashboardPeriod();
            int firstDayOfMonth = mSettingsProvider.firstDayOfMonth();

            switch (period) {
                case WEEKLY:
                    mAccountingPeriodManager = AccountingPeriodManager.weekly(Calendar.getInstance());
                    break;
                case BIWEEKLY:
                    mAccountingPeriodManager = AccountingPeriodManager.biweekly(Calendar.getInstance());
                    break;
                case MONTHLY:
                    mAccountingPeriodManager = AccountingPeriodManager.monthly(Calendar.getInstance(), firstDayOfMonth);
            }

            handler.post(() -> initialisationCompleted.call());

        }).start();

    }

    // Invoked by UI when user click to create a new Transaction
    public void newTransactionClicked() {
        clickNewTransactionEvent.call();
    }

    // Called by UI to obtain an AccountingPeriod
    public AccountingPeriod period(int index) {
        return mAccountingPeriodManager.period(index);
    }

    // Called by UI to obtain a title for a specific AccountingPeriod
    public String title(int pos, DateFormat dateFormat) {
        AccountingPeriod accountingPeriod = mAccountingPeriodManager.period(pos);
        Calendar temp = accountingPeriod.to();
        temp.add(Calendar.DAY_OF_MONTH, -1);

        return dateFormat.format(accountingPeriod.from().getTime()) + " - " +
            dateFormat.format(temp.getTime());
    }

    // Called by UI to find out how many AccountingPeriod's it need to display
    public int numberPeriods() {
        return mAccountingPeriodManager.numberPeriods();
    }

    // Called by UI to find out the index of the AccountingPeriod containing today
    public int indexOfToday() {
        return mAccountingPeriodManager.today();
    }
}
