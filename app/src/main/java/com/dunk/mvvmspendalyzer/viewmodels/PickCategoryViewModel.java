package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableList;
import android.support.annotation.StringRes;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.data.CategoryRepository;
import com.dunk.mvvmspendalyzer.domain.Category;
import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ViewModel to be used for a view where the user
 * may select a category from some kind of list.
 */
public class PickCategoryViewModel extends ViewModel {

    private CategoryRepository mCategoryRepository;

    // RxJava subscription are collected in mDisposable.
    // mDisposable is cleared in onCleared() to avoid memory leaks
    private CompositeDisposable mDisposable = new CompositeDisposable();

    // List of all categories
    public final ObservableList<Category> categories = new ObservableArrayList<>();

    // Indicates if data is loading
    public final ObservableBoolean dataLoading = new ObservableBoolean(false);

    // Event fired when a category has been selected
    public final SingleLiveEvent<Category> categorySelectedEvent = new SingleLiveEvent<>();

    // Event fired when an error message should be displayed
    public final SnackbarMessage showError = new SnackbarMessage();

    private boolean mDataLoaded;

    public PickCategoryViewModel(CategoryRepository categoryRepository) {
        mCategoryRepository = categoryRepository;
    }

    public void start() {
        if (dataLoading.get()) {
            return;
        }
        if (mDataLoaded) {
            return;
        }

        dataLoading.set(true);
        loadCategories();
    }

    private void loadCategories() {
        // This stream stays open and emit a new list each time a change occurs in underlying data
        mDisposable.add(mCategoryRepository.categories()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> categoriesLoaded(list), error -> handleException(R.string.category_pick_error_load, error)));
    }

    // Invoked when categories has been loaded
    private void categoriesLoaded(List<Category> newCategories) {
        categories.clear();
        categories.addAll(newCategories);
        dataLoading.set(false);
        mDataLoaded = true;
    }

    // Invoked by UI when a category has been clicked
    public void categoryClicked(Category category) {
        categorySelectedEvent.setValue(category);
    }

    private void handleException(@StringRes int errorMsg, Throwable t) {
        showError.setValue(errorMsg);
    }

    @Override
    public void onCleared() {
        mDisposable.clear();
    }
}
