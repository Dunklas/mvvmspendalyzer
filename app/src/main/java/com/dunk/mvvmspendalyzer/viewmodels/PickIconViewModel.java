package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableList;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.resources.AssetsProvider;
import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * ViewModel to be used for a view where the user
 * may select an icon from some kind of list.
 */
public class PickIconViewModel extends ViewModel {

    private AssetsProvider mAssetsProvider;

    // A list containing all available icons
    private List<String> mAllIcons = new ArrayList<>();

    // A list containing all currently displayed icons
    public final ObservableList<String> showedIcons = new ObservableArrayList<>();

    // Indicates if data is currently loading
    public final ObservableBoolean dataLoading = new ObservableBoolean(false);

    // Event fired when an icon has been selected
    public final SingleLiveEvent<String> iconSelectedEvent = new SingleLiveEvent<>();

    // Event fired when an error has occurred
    public final SnackbarMessage showError = new SnackbarMessage();

    private boolean mDataLoaded;

    public PickIconViewModel(AssetsProvider assetsProvider) {
        mAssetsProvider = assetsProvider;
    }

    // Invoked by UI layer to start/init this ViewModel
    public void start() {
        if (dataLoading.get()) {
            return;
        }
        if (mDataLoaded) {
            return;
        }

        dataLoading.set(true);
        loadIcons();
    }

    private void loadIcons() {
        mAssetsProvider.iconNames()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(i -> iconsLoaded(i), error -> handleException(R.string.icon_pick_error_load, error));
    }

    // Invoked when icons has been loaded
    private void iconsLoaded(String[] icons) {
        mAllIcons.clear();
        mAllIcons.addAll(Arrays.asList(icons));
        showedIcons.addAll(mAllIcons);

        dataLoading.set(false);
        mDataLoaded = true;
    }

    // Invoked by UI when search query has changed
    public void searchQueryChanged(String query) {
        final String queryLower = query.toLowerCase();
        showedIcons.clear();
        showedIcons.addAll(
                mAllIcons.stream()
                .filter((s) -> s.contains(queryLower))
                .collect(Collectors.toList())
        );
    }

    // Invoked by UI when an icon has been clicked
    public void iconClicked(String icon) {
        iconSelectedEvent.setValue(icon);
    }

    private void handleException(@StringRes int errorMsg, Throwable t) {
        showError.setValue(errorMsg);
    }
}
