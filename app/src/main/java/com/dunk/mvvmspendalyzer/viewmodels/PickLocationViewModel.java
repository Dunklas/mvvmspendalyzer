package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.domain.Coordinate;
import com.dunk.mvvmspendalyzer.logging.Tags;
import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;
import com.dunk.mvvmspendalyzer.ui.SnackbarAction;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;
import com.dunk.mvvmspendalyzer.ui.picklocation.PickLocationError;

/**
 * ViewModel to be used for a view where the user
 * may select a geographical location.
 *
 * This is extremely tightly coupled to the Android framework and its logic
 * for handling permissions and location data. Before, all this logic took place
 * in a Fragment, but I think the sequence of events is more visible if the logic
 * is driven by this ViewModel.
 *
 * I found it quite troublesome with the whole location/permission-flow
 * and therefore want full transparency. Therefore, this class is a bit "over-logged".
 */
public class PickLocationViewModel extends ViewModel {

    // Holds the currently selected location
    public final MutableLiveData<Coordinate> location = new MutableLiveData<>();
    private Observer locationObserver;

    // Event fired when user has confirmed the selected location and want to leave this view (in an affirmative way)
    public final SingleLiveEvent<Coordinate> locationSelected = new SingleLiveEvent<>();
    // Event fired when user want to search for a place
    public final SingleLiveEvent<Void> searchPlace = new SingleLiveEvent<>();

    // Event to manage the sequence of events during permission/location handling
    public final SingleLiveEvent<Void> verifyPermissions = new SingleLiveEvent<>();
    public final SingleLiveEvent<Void> shouldRationaleBeDisplayed = new SingleLiveEvent<>();
    public final SingleLiveEvent<Void> requestPermissions = new SingleLiveEvent<>();
    public final SingleLiveEvent<Void> obtainCurrentLocation = new SingleLiveEvent<>();

    // Event fired to show a rationale message
    public final SnackbarAction showRationale = new SnackbarAction(
            R.string.location_pick_permission_rationale,
            R.string.location_pick_permission_rationale_enable,
            () -> requestPermissions.call());

    // Event fired to display an error message
    public final SnackbarMessage showError = new SnackbarMessage();

    // Indicates if this "form" is valid (which it is if a location has been selected)
    public final MutableLiveData<Boolean> formValid = new MutableLiveData<>();

    // Indicates if the app is loading the current user location
    public final ObservableBoolean currentLocationLoading = new ObservableBoolean(false);

    public PickLocationViewModel() {
        formValid.setValue(false);

        // Observe the selected location in order to validate it and set currentLoadingData to false each time it changes
        locationObserver = loc -> {
            if (currentLocationLoading.get()) {
                currentLocationLoading.set(false);
            }
            validate();

            Log.i(Tags.LOG_UI, "Location set: " + location.getValue().latitude() + ", " + location.getValue().longitude());
        };
        location.observeForever(locationObserver);
    }

    // Invoked by UI layer to start/init this ViewModel
    public void start(Coordinate coord) {
        if (location.getValue() == null && coord != null) {
            location.setValue(coord);
        }
    }

    // Invoked by UI when map has been loaded
    public void onMapLoaded() {
        Log.i(Tags.LOG_UI, "Map loaded.");
        if (!isLocationSet()) {
            Log.i(Tags.LOG_UI, "No location set. Verifying location permissions.");
            verifyPermissions.call();
        }
    }

    // Invoked by UI when it has detected that location permissions are missing
    public void onMissingPermissions() {
        shouldRationaleBeDisplayed.call();
        Log.i(Tags.LOG_UI, "Missing location permissions.");
    }

    // Invoked by UI when it has detected that location permissions have previously been declined
    public void onRationaleShouldBeDisplayed() {
        Log.i(Tags.LOG_UI, "Location permissions has previously been declined. Showing rationale message.");
        showRationale.call();
    }

    // Invoked by UI when it has detected that location permissions have not been declined before
    public void onRationaleShouldNotBeDisplayed() {
        Log.i(Tags.LOG_UI, "Location permissions has not been declined before. Requesting location permissions.");
        requestPermissions.call();
    }

    // Invoked by UI when it has detected that location permissions have been granted earlier
    public void onHavePermissions() {
        Log.i(Tags.LOG_UI, "Have location permissions. Obtaining current location.");
        currentLocationLoading.set(true);
        obtainCurrentLocation.call();
    }

    // Invoked by UI when location permissions have been granted
    public void onPermissionsGranted() {
        Log.i(Tags.LOG_UI, "Location permissions granted. Obtaining current location.");
        currentLocationLoading.set(true);
        obtainCurrentLocation.call();
    }

    // Invoked by UI when location permissions has been declined
    public void onPermissionsDenied() {
        // Do nothing
    }

    // Invoked by UI when an error has occurred
    public void onError(@NonNull PickLocationError error, @Nullable Throwable exception) {
        switch (error) {
            case GOOGLE_PLAY_SERVICES_NOT_AVAILABLE:
                showError.setValue(R.string.location_pick_place_search_error);
                break;
            case GOOGLE_PLAY_SERVICES_NOT_AVAILABLE_REPAIRABLE:
                break;
            case PLACE_AUTO_COMPLETE_RESULT_ERROR:
                showError.setValue(R.string.location_pick_place_search_error);
                break;
        }
    }

    // Invoked by UI when a location has been selected
    public void selectLocation() {
        locationSelected.setValue(location.getValue());
    }

    // Invoked by UI when user has clicked to search for a place
    public void doPlaceSearch() {
        searchPlace.call();
    }

    // Returns a boolean indicating if location has been set
    public boolean isLocationSet() {
        return location.getValue() != null;
    }

    // Check if a location is set and set formValid to true (if location is set)
    private void validate() {
        if (location.getValue() != null) {
            formValid.setValue(true);
        }
    }

    @Override
    public void onCleared() {
        location.removeObserver(locationObserver);
    }
}
