package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.ViewModel;

import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;

/**
 * ViewModel to be used for a splash screen.
 * Any kind of initialization of the app may be done here.
 */
public class SplashViewModel extends ViewModel {

    // TODO: Replace these two events with an ObservableBoolean indicating if data is loading or not (to be consistent with rest of application)
    public final SingleLiveEvent<Void> startLoadingEvent = new SingleLiveEvent<>();
    public final SingleLiveEvent<Void> doneLoadingEvent = new SingleLiveEvent<>();

    // Event fired when default settings/preferences should be set (currently the only initialization that is performed in this application)
    public final SingleLiveEvent<Void> setDefaultSettings = new SingleLiveEvent<>();

    public void start() {
        loadApplication();
    }

    private void loadApplication() {
        startLoadingEvent.call();

        setDefaultSettings.call();

        doneLoadingEvent.call();
    }
}
