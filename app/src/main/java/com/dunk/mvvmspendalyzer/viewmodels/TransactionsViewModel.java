package com.dunk.mvvmspendalyzer.viewmodels;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableList;
import android.support.annotation.StringRes;

import com.dunk.mvvmspendalyzer.R;
import com.dunk.mvvmspendalyzer.data.TransactionRepository;
import com.dunk.mvvmspendalyzer.domain.Transaction;
import com.dunk.mvvmspendalyzer.ui.SingleLiveEvent;
import com.dunk.mvvmspendalyzer.ui.SnackbarMessage;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

/**
 * ViewModel used for a view where a list of
 * Transaction objects are displayed.
 */
public class TransactionsViewModel extends ViewModel {

    private TransactionRepository mTransactionRepository;

    // RxJava subscription are collected in mDisposable.
    // mDisposable is cleared in onCleared() to avoid memory leaks
    private CompositeDisposable mDisposable = new CompositeDisposable();

    public final ObservableList<Transaction> transactions = new ObservableArrayList<>();
    public final ObservableBoolean dataLoading = new ObservableBoolean(false);

    // Event fired when user click a transaction in the list
    public final SingleLiveEvent<String> clickExistingTransactionEvent = new SingleLiveEvent<>();

    // Event fired when user click to create a new transaction
    public final SingleLiveEvent<Void> clickNewTransactionEvent = new SingleLiveEvent<>();

    // Event fired when an error message should be displayed
    public final SnackbarMessage showError = new SnackbarMessage();

    private boolean mIsDataLoaded = false;

    public TransactionsViewModel(TransactionRepository transactionRepository) {
        mTransactionRepository = transactionRepository;
    }

    // Invoked by UI layer to start/init this ViewModel
    public void start() {

        if (dataLoading.get()) {
            return;
        }

        if (mIsDataLoaded) {
            return;
        }

        dataLoading.set(true);
        loadTransactions();
    }

    // Obtain Transaction objects from mTransactionRepository
    // A new list will be pushed to this stream each time a change is made in underlying data
    private void loadTransactions() {
        mDisposable.add(mTransactionRepository.transactions()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> transactionsLoaded(list), error -> handleException(R.string.transactions_error_load, error)));
    }

    // Called when a list of Transaction objects has been loaded
    private void transactionsLoaded(List<Transaction> transactions) {
        this.transactions.clear();
        this.transactions.addAll(transactions);
        dataLoading.set(false);
        mIsDataLoaded = true;
    }

    // Called by UI layer to fire this event
    public void existingTransactionClicked(String transactionId) {
        clickExistingTransactionEvent.setValue(transactionId);
    }

    // Called by UI layer to fire this event
    public void newTransactionClicked() {
        clickNewTransactionEvent.call();
    }

    private void handleException(@StringRes int errorMsg, Throwable t) {
        showError.setValue(errorMsg);
    }

    @Override
    protected void onCleared() {
        mDisposable.clear();
    }
}
