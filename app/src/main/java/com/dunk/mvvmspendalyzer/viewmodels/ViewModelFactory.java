package com.dunk.mvvmspendalyzer.viewmodels;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.dunk.mvvmspendalyzer.data.CategoryRepository;
import com.dunk.mvvmspendalyzer.data.CategoryRepositoryImpl;
import com.dunk.mvvmspendalyzer.data.TransactionRepository;
import com.dunk.mvvmspendalyzer.data.TransactionRepositoryImpl;
import com.dunk.mvvmspendalyzer.resources.AssetsProvider;
import com.dunk.mvvmspendalyzer.resources.AssetsProviderImpl;
import com.dunk.mvvmspendalyzer.resources.SettingsProvider;
import com.dunk.mvvmspendalyzer.resources.SettingsProviderImpl;

/**
 * A factory class used together with ViewModelProviders to instantiate
 * ViewModel objects. A default one could've been used as well, but a custom one
 * is used in order to inject ViewModel objects with dependencies (such as
 * CategoryRepository, TransactionRepository, etc).
 *
 * See:
 *  - https://developer.android.com/reference/android/arch/lifecycle/ViewModelProvider.html
 *  - https://developer.android.com/reference/android/arch/lifecycle/ViewModelProvider.Factory.html
 *  - https://developer.android.com/reference/android/arch/lifecycle/ViewModelProviders.html
 */
public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private static volatile ViewModelFactory INSTANCE;

    private final Application mApplication;

    private final AssetsProvider mAssetsProvider;

    private final SettingsProvider mSettingsProvider;

    private final CategoryRepository mCategoryRepository;

    private final TransactionRepository mTransactionRepository;

    public static ViewModelFactory getInstance(Application application) {

        if (INSTANCE == null) {
            synchronized (ViewModelFactory.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ViewModelFactory(application);
                }
            }
        }

        return INSTANCE;
    }

    private ViewModelFactory(Application application) {
        mApplication = application;

        mCategoryRepository = CategoryRepositoryImpl.getInstance(mApplication);
        mTransactionRepository = TransactionRepositoryImpl.getInstance(mApplication);
        mAssetsProvider = AssetsProviderImpl.getInstance(mApplication);
        mSettingsProvider = SettingsProviderImpl.getInstance(mApplication);
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(SplashViewModel.class)) {
            return (T) new SplashViewModel();
        } else if (modelClass.isAssignableFrom(CategoriesViewModel.class)) {
            return (T) new CategoriesViewModel(mCategoryRepository);
        } else if (modelClass.isAssignableFrom(TransactionsViewModel.class)) {
            return (T) new TransactionsViewModel(mTransactionRepository);
        } else if (modelClass.isAssignableFrom(DashboardViewModel.class)) {
            return (T) new DashboardViewModel(mSettingsProvider);
        } else if (modelClass.isAssignableFrom(AccountingPeriodViewModel.class)) {
            return (T) new AccountingPeriodViewModel(mTransactionRepository);
        } else if (modelClass.isAssignableFrom(AddEditCategoryViewModel.class)) {
            return (T) new AddEditCategoryViewModel(mCategoryRepository);
        } else if (modelClass.isAssignableFrom(AddEditTransactionViewModel.class)) {
            return (T) new AddEditTransactionViewModel(mTransactionRepository);
        } else if (modelClass.isAssignableFrom(PickIconViewModel.class)) {
            return (T) new PickIconViewModel(mAssetsProvider);
        } else if (modelClass.isAssignableFrom(PickCategoryViewModel.class)) {
            return (T) new PickCategoryViewModel(mCategoryRepository);
        } else if (modelClass.isAssignableFrom(PickLocationViewModel.class)) {
            return (T) new PickLocationViewModel();
        }

        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
